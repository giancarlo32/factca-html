// JavaScript Document
$(function() {
    $('[data-toggle="tooltip"]').tooltip()
})

$(document).on('mouseenter', ".iffyTip", function() {
    var $this = $(this);
    if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
        $this.tooltip({
            title: $this.text(),
            placement: "bottom"
        });
        $this.tooltip('show');
    }
});

/*login*/

var working = false;
$('.login').on('submit', function(e) {
    e.preventDefault();
    if (working) return;
    working = true;
    var $this = $(this),
        $state = $this.find('button > .state');
    $this.addClass('loading');
    $state.html('verificando');
    setTimeout(function() {
        $this.addClass('ok');
        $state.html('bienvenido!');
        setTimeout(function() {
            $state.html('entrar');
            $this.removeClass('ok loading');
            working = false;
        }, 4000);
    }, 3000);
});


/*wizard*/
(function() {

    // Variables
    //var currentSlide = global.currentSlide,
    slideName = $('div.slide'),
        totalSlides = slideName.length,
        slideCounter = $('div.slide-counter'),
        btnNext = $('a#btn-next'),
        btnPrev = $('a#btn-prev'),
        addSlide = $('a#add-slide');

    slideCounter.text(global.currentSlide + ' / ' + totalSlides);


    // Slide Transitions
    function btnTransition(button, direction) {

        $(button).on('click', function() {

            if (button === btnNext && global.currentSlide >= totalSlides) {
                global.currentSlide = 1;
            } else if (button === btnPrev && global.currentSlide === 1) {
                global.currentSlide = totalSlides;
            } else {
                direction();
            };

            slideName.filter('.active').animate({
                opacity: 0,
                left: -40
            }, 400, function() {
                $(this)
                    .removeClass('active')
                    .css('left', 0);
                $(slideName)
                    .eq(global.currentSlide - 1)
                    .css({
                        'opacity': 0,
                        'left': 40
                    })
                    .addClass('active')
                    .animate({
                        opacity: 1,
                        left: 0
                    }, 400);

            });

            slideCounter.text(global.currentSlide + ' / ' + totalSlides);

        });
    };


    // Slide forward
    btnTransition(btnNext, function() {
        global.currentSlide++;
        goconfiguracion(global.currentSlide);
    });


    // Slide Backwards
    btnTransition(btnPrev, function() {
        global.currentSlide--;
        goconfiguracion(global.currentSlide);
    });


    // Add a Slide
    $(addSlide).on('click', function() {

    });

})();

/*STEP*/
var $container = $('.step');
var $p = $('p');
var currentIndex = 0;

$p.on('click', function(e) {
    var $current = $(e.currentTarget);
    var index = $p.index($current);
    if (index > currentIndex) {
        $container.addClass('forward');
    } else {
        $container.removeClass('forward');
    }
    currentIndex = index;
    $container.attr('data-step', index);
});