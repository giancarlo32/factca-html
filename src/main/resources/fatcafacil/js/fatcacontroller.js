function FatcaController() {
    //var cs= new ConfiguracionService();
}




FatcaController.prototype.validarconfiguracion = function() {
    var cs = new ConfiguracionService();
    cs.actualizar_tablas(function(mensaje) {
        //alert(mensaje);
        cs.estadoConfiguracion(function(r) {

            if (r === 3) {
                if (global.isLogged === true) {
                    //window.location.replace("index.html");    
                    load_main();

                } else {
                    //window.location.replace("login.html");    
                    $("#workspace").load("html/login.html", function() {});
                }

            } else if (r === 1) {
                $("#workspace").load("html/configuracion.html", function() {


                    $("#b_login").css("display", "none");
                    global.isLogged = false;
                    goconfiguracion(1);

                    cs.getListaPais(function(data) {

                        if (data.rows.length > 0) {
                            $('#pais').empty();

                            for (i = 0; i < data.rows.length; i++) {
                                $("#pais").append("<option data-icon='" + data.rows.item(i).BANDERA + "'' value=" + data.rows.item(i).CODIGOPAIS + ">" + data.rows.item(i).NOMBRE + "</option>");

                            }
                        }
                        $('#pais').change();
                        $('#pais').wSelect();

                    });

                });
            }

        });

        //FatcaController.prototype.validarconfiguracion();//recursivo
    });

};


FatcaController.prototype.cargarConfiguracion = function() {
    console.log("cargarConfiguracion");
    var gs = new GiinService();
    gs.listagiin(function(data) {


        $("#combobox,#comboboxin,#comboboxspon").find('option').remove().end();
        for (i = 0; i < data.rows.length; i++) {
            $("#combobox,#comboboxin,#comboboxspon").append('<option value=' + data.rows.item(i).codgiin + '>' + data.rows.item(i).nombreentidad + '</option>');
        }

        var cs = new ConfiguracionService();
        cs.cargarConfiguracion(function(r) {
            console.log("-------------------");
            console.log(JSON.stringify(r));
            console.log("-------------------");



            if (jQuery.isEmptyObject(r)) {

                $("#check_smv_1").prop("checked", false);
                $("#check_smv_2").prop("checked", false);
                $("#check_smv_3").prop("checked", false);


                $("#l_entidad_financiera").val("Haga click para configurar la entidad financiera");
                $("#l_entidad_logo").val("Haga click para configurar la entidad financiera");
                fc.getDepartamentos();
                $("#select_pro").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                $("#select_dis").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                $("#select_pro1").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                $("#select_dis1").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                $("#select_pro2").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                $("#select_dis2").find('option').remove().end().append('<option value=0>SELECCIONE</option>');



                $(".combo1").find('span').find('input').val("");
                $("#entidadcodgiin").val("");
                $("#entidadcalle").val("");
                $("#entidadnumero").val("");
                $("#entidadpiso").val("");
                $("#entidadoficina").val("");

                $("#check_intermediario").prop("checked", false);
                $(".combo2").find('span').find('input').val("");
                $("#intercodgiin").val("");
                $("#intercalle").val("");
                $("#internumero").val("");
                $("#interpiso").val("");
                $("#interoficina").val("");

                $("#check_sponsor").prop("checked", false);
                $(".combo3").find('span').find('input').val("");
                $("#sponcodgiin").val("");
                $("#sponcalle").val("");
                $("#sponnumero").val("");
                $("#sponpiso").val("");
                $("#sponoficina").val("");

                $("#licencia").val("");


                if ($('#check_intermediario').prop('checked') == true) {
                    $('#li_tab_default_2').css('display', 'block');
                } else {
                    $('#li_tab_default_2').css('display', 'none');
                }
                if ($('#check_sponsor').prop('checked') == true) {
                    $('#li_tab_default_3').css('display', 'block');
                } else {
                    $('#li_tab_default_3').css('display', 'none');
                }



            } else {
                if (r.financiera.smv == 1)
                    $("#check_smv_1").prop("checked", true);
                else
                    $("#check_smv_1").prop("checked", false);

                $("#l_entidad_financiera").val(r.financiera.nombreentidad);
                $("#l_entidad_logo").val(r.financiera.nombreentidad);

                //$("#l_nombres_usuario").html(global.username);


                //entidad1['nombreentidad']=$("#combobox option:selected").text();

                //$("#combobox option:contains(" + r.financiera.nombreentidad + ")").attr('selected', 'selected');
                $("#combobox option[value='" + r.financiera.codgiin + "']").attr('selected', 'selected');
                $(".combo1").find('span').find('input').val(r.financiera.nombreentidad);




                $("#entidadcodgiin").val(r.financiera.codgiin);
                //entidad1['paisorigen']=$("#paisfinanciera").val();
                //entidad1['dirciudad']=$("#select_depa option:selected").text();
                //entidad1['dirprovincia']=$("#select_pro option:selected").text();
                //entidad1['dirdistrito']=$("#select_dis option:selected").text();*/
                $("#entidadcalle").val(r.financiera.calle);
                $("#entidadnumero").val(r.financiera.numero);
                $("#entidadpiso").val(r.financiera.piso);
                $("#entidadoficina").val(r.financiera.oficina);
                var filer = r.financiera.filer;
                console.log(filer);
                if(filer === undefined || filer === null ){
                    filer = "FATCA601";
                    console.log("undef aqui ");
                }

                $("#i_filer").val(filer);
                //$("#select_depa option:contains(" + r.financiera.dirciudad + ")").attr('selected', 'selected');

                if (r.financiera.dirciudad === "") {
                    $("#select_depa").val('0');
                }

                if (r.financiera.dirprovincia === "") {
                    $("#select_pro").val('0');
                }

                if (r.financiera.dirdistrito === "") {
                    $("#select_dis").val('0');
                }

                var us = new UbicacionService();
                if (global.pais == "PE") {
                    us.getDepartamentos(function(data) {
                        $('#select_depa').find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                        for (i = 0; i < data.rows.length; i++) {
                            $("#select_depa").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDEP + '</option>');
                        }
                        if (r.financiera.dirciudad != "")
                            $("#select_depa option:contains(" + r.financiera.dirciudad + ")").attr('selected', 'selected');
                        us.getProvincias(function(data) {
                            $("#select_pro").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                            for (i = 0; i < data.rows.length; i++) {
                                $("#select_pro").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMPRO + '</option>');
                            }
                            if (r.financiera.dirprovincia != "")
                                $("#select_pro option:contains(" + r.financiera.dirprovincia + ")").attr('selected', 'selected');
                            us.getDistritos(function(data) {
                                $("#select_dis").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                                for (i = 0; i < data.rows.length; i++) {
                                    $("#select_dis").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDIS + '</option>');
                                }
                                if (r.financiera.dirdistrito != "")
                                    $("#select_dis option:contains(" + r.financiera.dirdistrito + ")").attr('selected', 'selected');
                            }, $("#select_pro").val().substring(0, 4));
                        }, $("#select_depa").val().substring(0, 2));
                    });
                }

                if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
                    us.getDepartamentos(function(data) {
                        {
                            $("#select_depa").val(r.financiera.dirciudad);
                        }
                        us.getProvincias(function(data) {
                            $("#select_pro").val(r.financiera.dirprovincia);
                            us.getDistritos(function(data) {
                                $("#select_dis").val(r.financiera.dirdistrito);
                            });
                        });
                    });
                }


                if (r.intermediario.smv == 1)
                    $("#check_smv_2").prop("checked", true);
                else
                    $("#check_smv_2").prop("checked", false);

                $("#check_intermediario").prop("checked", true);
                $("#comboboxin option:contains(" + r.intermediario.nombreentidad + ")").attr('selected', 'selected');
                $(".combo2").find('span').find('input').val(r.intermediario.nombreentidad);
                //entidad2['nombreentidad']=$("#comboboxin option:selected").text();
                $("#intercodgiin").val(r.intermediario.codgiin);
                //entidad2['paisorigen']=$("#paisfinanciera").val();
                //entidad2['dirciudad']=$("#select_depa1 option:selected").text();
                //entidad2['dirprovincia']=$("#select_pro1 option:selected").text();
                //entidad2['dirdistrito']=$("#select_dis1 option:selected").text();*/
                $("#intercalle").val(r.intermediario.calle);
                $("#internumero").val(r.intermediario.numero);
                $("#interpiso").val(r.intermediario.piso);
                $("#interoficina").val(r.intermediario.oficina);
                if (r.intermediario.nombreentidad != "") {

                    if (global.pais == "PE") {


                        if (r.intermediario.dirciudad === "") {
                            $("#select_depa1").val('0');
                        }

                        if (r.intermediario.dirprovincia === "") {
                            $("#select_pro1").val('0');
                        }

                        if (r.intermediario.dirdistrito === "") {
                            $("#select_dis1").val('0');
                        }

                        us.getDepartamentos(function(data) {
                            $('#select_depa1').find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                            for (i = 0; i < data.rows.length; i++) {
                                $("#select_depa1").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDEP + '</option>');
                            }
                            if (r.intermediario.dirciudad != "")
                                $("#select_depa1 option:contains(" + r.intermediario.dirciudad + ")").attr('selected', 'selected');
                            us.getProvincias(function(data) {
                                $("#select_pro1").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                                for (i = 0; i < data.rows.length; i++) {
                                    $("#select_pro1").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMPRO + '</option>');
                                }
                                if (r.intermediario.dirprovincia != "")
                                    $("#select_pro1 option:contains(" + r.intermediario.dirprovincia + ")").attr('selected', 'selected');
                                us.getDistritos(function(data) {
                                    $("#select_dis1").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                                    for (i = 0; i < data.rows.length; i++) {
                                        $("#select_dis1").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDIS + '</option>');
                                    }
                                    if (r.intermediario.dirdistrito != "")
                                        $("#select_dis1 option:contains(" + r.intermediario.dirdistrito + ")").attr('selected', 'selected');
                                }, $("#select_pro1").val().substring(0, 4));
                            }, $("#select_depa1").val().substring(0, 2));
                        });
                    }

                    if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
                        us.getDepartamentos(function(data) {
                            {
                                $("#select_depa1").val(r.intermediario.dirciudad);
                            }
                            us.getProvincias(function(data) {
                                $("#select_pro1").val(r.intermediario.dirprovincia);
                                us.getDistritos(function(data) {
                                    $("#select_dis1").val(r.intermediario.dirdistrito);
                                });
                            });
                        });
                    }

                } else {
                    $("#check_intermediario").prop("checked", false);
                    $("#comboboxin").val("");
                    if (global.pais == "PE") {
                        $("#select_depa1").val(0);
                        $("#select_pro1").val(0);
                        $("#select_dis1").val(0);
                    }
                    if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
                        $("#select_depa1").val("");
                        $("#select_pro1").val("");
                        $("#select_dis1").val("");
                    }
                }


                if (r.sponsor.smv == 1)
                    $("#check_smv_3").prop("checked", true);
                else
                    $("#check_smv_3").prop("checked", false);


                $("#comboboxspon option:contains(" + r.sponsor.nombreentidad + ")").attr('selected', 'selected');
                $(".combo3").find('span').find('input').val(r.sponsor.nombreentidad);
                //entidad2['nombreentidad']=$("#comboboxspon option:selected").text();
                $("#sponcodgiin").val(r.sponsor.codgiin);
                //entidad2['paisorigen']=$("#paisfinanciera").val();
                //entidad2['dirciudad']=$("#select_depa2 option:selected").text();
                //entidad2['dirprovincia']=$("#select_pro2 option:selected").text();
                //entidad2['dirdistrito']=$("#select_dis2 option:selected").text();*/
                $("#sponcalle").val(r.sponsor.calle);
                $("#sponnumero").val(r.sponsor.numero);
                $("#sponpiso").val(r.sponsor.piso);
                $("#sponoficina").val(r.sponsor.oficina);
                
                var filer = r.sponsor.filer;
                console.log(filer);
                if(filer === undefined || filer === null ){
                    filer = "FATCA607";
                    console.log("undef aqui ");
                }


                $("#i_filer_sponsor").val(filer);


                if (r.sponsor.nombreentidad != "") {
                    $("#check_sponsor").prop("checked", true);
                    if (global.pais == "PE") {

                        if (r.sponsor.dirciudad === "") {
                            $("#select_depa1").val('0');
                        }

                        if (r.sponsor.dirprovincia === "") {
                            $("#select_pro1").val('0');
                        }

                        if (r.sponsor.dirdistrito === "") {
                            $("#select_dis1").val('0');
                        }

                        us.getDepartamentos(function(data) {
                            $('#select_depa2').find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                            for (i = 0; i < data.rows.length; i++) {
                                $("#select_depa2").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDEP + '</option>');
                            }
                            if (r.sponsor.dirciudad != "")
                                $("#select_depa2 option:contains(" + r.sponsor.dirciudad + ")").attr('selected', 'selected');
                            us.getProvincias(function(data) {
                                $("#select_pro2").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                                for (i = 0; i < data.rows.length; i++) {
                                    $("#select_pro2").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMPRO + '</option>');
                                }
                                if (r.sponsor.dirprovincia != "")
                                    $("#select_pro2 option:contains(" + r.sponsor.dirprovincia + ")").attr('selected', 'selected');
                                us.getDistritos(function(data) {
                                    $("#select_dis2").find('option').remove().end().append('<option value=0>SELECCIONE</option>');
                                    for (i = 0; i < data.rows.length; i++) {
                                        $("#select_dis2").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDIS + '</option>');
                                    }
                                    if (r.sponsor.dirdistrito != "")
                                        $("#select_dis2 option:contains(" + r.sponsor.dirdistrito + ")").attr('selected', 'selected');
                                }, $("#select_pro2").val().substring(0, 4));
                            }, $("#select_depa2").val().substring(0, 2));



                        });

                    }

                    if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
                        us.getDepartamentos(function(data) {
                            {
                                $("#select_depa2").val(r.sponsor.dirciudad);
                            }
                            us.getProvincias(function(data) {
                                $("#select_pro2").val(r.sponsor.dirprovincia);
                                us.getDistritos(function(data) {
                                    $("#select_dis2").val(r.sponsor.dirdistrito);
                                });
                            });
                        });
                    }


                } else {
                    $("#check_sponsor").prop("checked", false);
                    $("#comboboxspon").val("");

                    if (global.pais == "PE") {
                        $("#select_depa2").val(0);
                        $("#select_pro2").val(0);
                        $("#select_dis2").val(0);
                    }
                    if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
                        $("#select_depa2").val("");
                        $("#select_pro2").val("");
                        $("#select_dis2").val("");
                    }
                }

                if ($('#check_intermediario').prop('checked') == true) {
                    $('#li_tab_default_2').css('display', 'block');
                } else {
                    $('#li_tab_default_2').css('display', 'none');
                }
                if ($('#check_sponsor').prop('checked') == true) {
                    $('#li_tab_default_3').css('display', 'block');
                } else {
                    $('#li_tab_default_3').css('display', 'none');
                }


            }
        });


    });




}

FatcaController.prototype.crearTablas = function() {
    var cs = new ConfiguracionService();
    cs.crearTablas(function(data) {
        mensaje(data, 1);
    });
}

FatcaController.prototype.login = function(user, password) {
    global.isLogged = true;
    window.location.replace("index.html");
}


FatcaController.prototype.checkUsuario = function(iusuario, ipassword) {
    var ud = new UsuarioService();
    ud.checkUsuario(function(data) {
        if (data === true) {
            global.isLogged = true;

            global.username = iusuario;
            console.log("correctamente logeado");

            ud.getUsuario(function(data) {
                global.pais = data.rows.item(0).pais;
                //alert(global.pais); 
                global.nombres = data.rows.item(0).nombres;
                global.idusuario = data.rows.item(0).id;
                global.showayuda = data.rows.item(0).showayuda;
                //alert(global.showayuda);
                ud.updateShowAyuda(function() {}, iusuario, 'false');

                load_main();
            }, iusuario);


        } else {
            //window.location.replace("login.html");
            //alert("fallo las credenciasles");
            mensajeFatcaFacil("Usuario o Password incorrectos, digite nuevamente.", 3);
            //$("#workspace").load("html/login.html", function() {

            //});

        }
    }, iusuario, ipassword);
}
FatcaController.prototype.setUsuario = function(iusuario, ipassword, inombre, iapellidos, pais, modelo, contacto, emailcontacto) {
    //alert("controller ")
    var ud = new UsuarioService();


    ///////////////////////////////////////////////
    ///////////////////////////////////////////////



    ud.getUsuario(function(data) {
        console.log(data);
        if (data.rows.length > 0) {
            mensaje("El Usuario ya ha sido creado", 2);
        } else {
            ud.setUsuario(function(data) {
                $("#workspace").load("html/login.html", function() {
                    mensajeFatcaFacil("Usuario creado correctamente.", 1);
                });
            }, iusuario, ipassword, inombre, iapellidos, pais, modelo, contacto, emailcontacto);
        }
    }, iusuario);

    ///////////////////////////////////////////////
    ///////////////////////////////////////////////


}

FatcaController.prototype.updatePassUsuario = function(ipassword, iusuario, newpassword, renewpassword) {
    //alert("controller ")

    console.log(ipassword);
    console.log(iusuario);
    console.log(newpassword);
    console.log(renewpassword);


    var ud = new UsuarioService();
    if (renewpassword == newpassword) {
        ud.updatePassUsuario(function(data) {
            //process.mainModule.exports.isLogged=true;
            mensaje(data, 1);
            //window.location.replace("login.html");
        }, iusuario, ipassword, newpassword);
    } else {
        mensajeFatcaFacil('Error al digitar la nueva contrase&ntilde;a : Deben ser iguales en ambos campos', 3);
    }
}
FatcaController.prototype.getPassUsuario = function(iusuario, ipassword) {

    var ud = new UsuarioService();
    ud.getPassUsuario(function(data) {

    }, iusuario, ipassword);
}

/*FatcaController.prototype.listagiin=  function(){
    var gs= new GiinService();
          gs.listagiin(function(data){
                    for (i = 0; i < data.rows.length; i++) {
                      $("#combobox,#comboboxin,#comboboxspon").append('<option value=' + data.rows.item(i).codgiin + '>' + data.rows.item(i).nombreentidad+ '</option>');
                    
                    }           
          });
}*/


FatcaController.prototype.getDepartamentos = function() {


    var us = new UbicacionService();
    us.getDepartamentos(function(data) {
        $('#select_depa,#select_depa1,#select_depa2').find('option').remove().end().append('<option value=0>SELECCIONE</option>');

        for (i = 0; i < data.rows.length; i++) {
            $("#select_depa,#select_depa1,#select_depa2").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDEP + '</option>');
        }
    });
}


FatcaController.prototype.getProvincias = function(filter, target) {
    var us = new UbicacionService();
    us.getProvincias(function(data) {
        target.find('option').remove().end().append('<option value=0>SELECCIONE</option>');
        for (i = 0; i < data.rows.length; i++) {
            target.append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMPRO + '</option>');
        }
    }, filter.substring(0, 2));
}

FatcaController.prototype.getDistritos = function(filter, target) {
    var us = new UbicacionService();
    us.getDistritos(function(data) {
        target.find('option').remove().end().append('<option value=0>SELECCIONE</option>');
        for (i = 0; i < data.rows.length; i++) {
            target.append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDIS + '</option>');
        }
    }, filter.substring(0, 4));
}




FatcaController.prototype.guardarEntidades = function() {

    var mensaje_validacion = "";

    jsonObj = [];

    //alert($("#check_intermediario").prop('checked'));
    //alert($("#check_sponsor").prop('checked'));



    entidad1 = {}; //Financiera
    //entidad1['nombreentidad']=$("#combobox option:selected").text();
    entidad1['nombreentidad'] = $(".combo1").find('span').find('input').val();
    entidad1['codgiin'] = $("#entidadcodgiin").val();
    entidad1['paisorigen'] = $("#paisfinanciera").val();

    if( $("#i_filer").val() !== 'NINGUNO'){
         entidad1['filer'] = $("#i_filer").val();
    }
   



        if ($("#select_pro option:selected").text() == "SELECCIONE") {
            entidad1['dirprovincia'] = "";
        } else {
            entidad1['dirprovincia'] = $("#select_pro option:selected").text();
        }

        if($("#select_pro").val()!=""){
            entidad1['dirprovincia'] = $("#select_pro").val();
        }
  
    var keys = Object.keys(entidad1);
    for (i = 0; i < keys.length; i++) {
        if (entidad1[keys[i]] == "" || entidad1[keys[i]] == "SELECCIONE")
            mensaje_validacion += "<li>Financiera: " + keys[i] + "</li>";
    }

    entidad1['smv'] = 0;

    if (global.pais == "PE") {

        if ($("#select_depa option:selected").text() == "SELECCIONE") {
            entidad1['dirciudad'] = "";
        } else {
            entidad1['dirciudad'] = $("#select_depa option:selected").text();
        }

        /*
        if ($("#select_pro option:selected").text() == "SELECCIONE") {
            entidad1['dirprovincia'] = "";
        } else {
            entidad1['dirprovincia'] = $("#select_pro option:selected").text();
        }*/


        if ($("#select_dis option:selected").text() == "Seleccione" || $("#select_dis option:selected").text() == "SELECCIONE") {
            entidad1['dirdistrito'] = "";
        } else {
            entidad1['dirdistrito'] = $("#select_dis option:selected").text();
        }

        if ($("#check_smv_1").prop('checked') == true) {
            entidad1['smv'] = 1;
        }


    }
    if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
        entidad1['dirciudad'] = $("#select_depa").val();
        //entidad1['dirprovincia'] = $("#select_pro").val();
        entidad1['dirdistrito'] = $("#select_dis").val();

    }



    entidad1['calle'] = $("#entidadcalle").val();
    entidad1['numero'] = $("#entidadnumero").val();
    entidad1['piso'] = $("#entidadpiso").val();
    entidad1['oficina'] = $("#entidadoficina").val();
    entidad1['tipoentidad'] = 1;
    jsonObj.push(entidad1);



    if ($("#check_intermediario").prop('checked') == true) {
        entidad2 = {}; //Intermediaria
        //entidad2['nombreentidad']=$("#comboboxin option:selected").text();
        entidad2['nombreentidad'] = $(".combo2").find('span').find('input').val();
        entidad2['codgiin'] = $("#intercodgiin").val();
        entidad2['paisorigen'] = $("#paisintermediario").val();
       

         console.log("entidad 2 ******************");
         console.log($("#select_pro1 option:selected").text());


        if ($("#select_pro1 option:selected").text() == "SELECCIONE") {
                entidad2['dirprovincia'] = "";
        } else {
            entidad2['dirprovincia'] = $("#select_pro1 option:selected").text();
        }

        if($("#select_pro1").val() != ""){
             entidad2['dirprovincia'] = $("#select_pro1").val();
        }
           


        keys = Object.keys(entidad2);
        for (i = 0; i < keys.length; i++) {
            if (entidad2[keys[i]] == "" || entidad2[keys[i]] == "SELECCIONE")
                mensaje_validacion += "<li>Intermediario: " + keys[i] + "</li>";
        }
        entidad2['smv'] = 0;

        if (global.pais == "PE") {
            if ($("#select_depa1 option:selected").text() == "SELECCIONE") {
                entidad2['dirciudad'] = "";
            } else {
                entidad2['dirciudad'] = $("#select_depa1 option:selected").text();
            }
            /*
            if ($("#select_pro1 option:selected").text() == "SELECCIONE") {
                entidad2['dirprovincia'] = "";
            } else {
                entidad2['dirprovincia'] = $("#select_pro1 option:selected").text();
            }*/

            if ($("#select_dis1 option:selected").text() == "SELECCIONE" || $("#select_dis1 option:selected").text() == "Seleccione") {
                entidad2['dirdistrito'] = "";
            } else {
                entidad2['dirdistrito'] = $("#select_dis1 option:selected").text();
            }

            if ($("#check_smv_2").prop('checked') == true) {
                entidad2['smv'] = 1;
            }

        }
        if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
            entidad2['dirciudad'] = $("#select_depa1").val();
            //entidad2['dirprovincia'] = $("#select_pro1").val();
            entidad2['dirdistrito'] = $("#select_dis1").val();
        }

        entidad2['calle'] = $("#intercalle").val();
        entidad2['numero'] = $("#internumero").val();
        entidad2['piso'] = $("#interpiso").val();
        entidad2['tipoentidad'] = 2;
        entidad2['oficina'] = $("#interoficina").val();
        jsonObj.push(entidad2);

    }

    //alert($("#check_sponsor").prop('checked'));




    if ($("#check_sponsor").prop('checked') == true) {
        entidad3 = {}; //Sponsor
        //entidad3['nombreentidad']=$("#comboboxspon option:selected").text();
        entidad3['nombreentidad'] = $(".combo3").find('span').find('input').val();
        entidad3['codgiin'] = $("#sponcodgiin").val();
        entidad3['paisorigen'] = $("#paissponsor").val();

        if( $("#i_filer_sponsor").val() != 'NINGUNO'){
             entidad3['filer'] = $("#i_filer_sponsor").val();
        }
       

        console.log("entidad 3 ******************");
         console.log($("#select_depa2 option:selected").text() );

        if ($("#select_pro2 option:selected").text() == "SELECCIONE") {
                entidad3['dirprovincia'] = "";
        } else {
            entidad3['dirprovincia'] = $("#select_pro2 option:selected").text();
        }

        if($("#select_pro2").val() != ""){
            entidad3['dirprovincia'] = $("#select_pro2").val();
        }

        keys = Object.keys(entidad3);
        for (i = 0; i < keys.length; i++) {
            if (entidad3[keys[i]] == "" || entidad3[keys[i]] == "SELECCIONE")
                mensaje_validacion += "<li>Sponsor: " + keys[i] + "</li>";
        }
        entidad3['smv'] = 0;

        if (global.pais == "PE") {

            if ($("#select_depa2 option:selected").text() == "SELECCIONE") {
                entidad3['dirciudad'] = "";
            } else {
                entidad3['dirciudad'] = $("#select_depa2 option:selected").text();
            }
            /*
            if ($("#select_pro2 option:selected").text() == "SELECCIONE") {
                entidad3['dirprovincia'] = "";
            } else {
                entidad3['dirprovincia'] = $("#select_pro2 option:selected").text();
            }*/

            if ($("#select_dis2 option:selected").text() == "SELECCIONE" || $("#select_dis2 option:selected").text() == "Seleccione") {
                entidad3['dirdistrito'] = "";
            } else {
                entidad3['dirdistrito'] = $("#select_dis2 option:selected").text();
            }

            if ($("#check_smv_3").prop('checked') == true) {
                entidad3['smv'] = 1;
            }

        }
        if (global.pais == "PA" || global.pais == "CL" || global.pais == "UY" || global.pais == "BB" || global.pais == "BO" || global.pais == "BM" || global.pais == "KY") {
            entidad3['dirciudad'] = $("#select_depa2").val();
            //entidad3['dirprovincia'] = $("#select_pro2").val();
            entidad3['dirdistrito'] = $("#select_dis2").val();
        }

        entidad3['calle'] = $("#sponcalle").val();
        entidad3['numero'] = $("#sponnumero").val();
        entidad3['piso'] = $("#sponpiso").val();
        entidad3['oficina'] = $("#sponoficina").val();
        entidad3['tipoentidad'] = 3;
        jsonObj.push(entidad3);

    }

    console.log(JSON.stringify(jsonObj));
    //alert(jsonObj[0].nombre);
    if (mensaje_validacion != "") {
        mensaje_validacion = "<div> tiene que validar los siguientes campos</div>" + "<div><ul>" + mensaje_validacion + "</ul><div>";
        mensajeFatcaFacil(mensaje_validacion, 3);

    } else {
        $('#my-Modal-fatca').modal('toggle');
        var cs = new ConfiguracionService();

        //alert(global.idusuario);
        cs.insertar_entidades(function(data) {
            //alert(data);
            FatcaController.prototype.insertar_configuracion();
        }, jsonObj, global.idusuario);
    }
}
FatcaController.prototype.updateConfiguracion = function() {
    $('#certificado_actual').val('');
    $('#passwordkey').val('');

    var cs = new ConfiguracionService();



    cs.select_configuracion(function(data) {


        $("#i_modelo").val(data.rows.item(0).modelo);
        $("#i_contacto").val(data.rows.item(0).contacto);
        $("#i_emailcontacto").val(data.rows.item(0).emailcontacto);
        global.tipoModelo = data.rows.item(0).modelo;
        var canvas = document.getElementById('imageCanvas');
        var ctx = canvas.getContext('2d');


        if (data.rows.length > 0) {
            data.rows.item(0).logo;
            var img = new Image();
            if (data.rows.item(0).logo != undefined)
                img.src = data.rows.item(0).logo;




            if (data.rows.item(0).pk1 != undefined)
                $("#passwordkey").val(data.rows.item(0).pk1);

            if (data.rows.item(0).pp1 != undefined)
                $("#passwordkeystore").val(data.rows.item(0).pp1);

            if (data.rows.item(0).LICENCIA != undefined)
                $("#licencia").val(data.rows.item(0).LICENCIA);

            if (data.rows.item(0).certificado != undefined)
                $("#certificado_actual").val(data.rows.item(0).certificado);


            read_alias_from_pfx(function() {
                $("#alias").val(0);
                if (data.rows.item(0).alias != undefined && data.rows.item(0).alias != "")
                    $("#alias option:contains(" + data.rows.item(0).alias + ")").attr('selected', 'selected');

            });


            img.onload = function() {

                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img, 0, 0);
            }
        } else {
            $("#alias").val("");
            $("#passwordkey").val("");
            $("#passwordkeystore").val("");
            $("#licencia").val("");

            //$("#certificado_actual").val("");
        }

        if ($("#certificado_actual").val() == "") {
            $("#certificado_actual").val("Haga click para seleccionar el certificado");
        }

        mostrarestadolicencia(cs);

    });
}

FatcaController.prototype.insertar_configuracion = function() {

    var canvas = document.getElementById('imageCanvas');
    var ctx = canvas.getContext('2d');
    var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);

    configuracion = {}; //Financiera
    configuracion['id'] = 1;
    configuracion['DIRECTORIOTRABAJO'] = "-";


    if ($('#alias').val() != 0)
        configuracion['alias'] = $('#alias option:selected').text();
    else
        configuracion['alias'] = "";


    configuracion['pk1'] = $("#passwordkey").val();
    configuracion['pp1'] = ""; /*$("#passwordkeystore").val();*/
    configuracion['logo'] = canvas.toDataURL();

    configuracion['licencia'] = $("#licencia").val()
    configuracion['actualizar_certificado'] = $('#actualizar_certificado').val();




    var cs = new ConfiguracionService();
    if (!isNulo($("#i_contacto").val()) && !isNulo($("#i_emailcontacto").val())) {
        cs.select_configuracion(function(c) {

            console.log(c.rows.item(0));
            //alert('dasda');
            configuracion['id'] = c.rows.item(0).id;
            configuracion['idusuario'] = c.rows.item(0).idusuario;
            configuracion['modelo'] = $("#i_modelo").val();
            configuracion['pais'] = c.rows.item(0).pais;
            configuracion['contacto'] = $("#i_contacto").val();
            configuracion['emailcontacto'] = $("#i_emailcontacto").val();
            if (c.rows.item(0).certificado != undefined) {
                configuracion['tipocertificado'] = c.rows.item(0).certificado;
            } else {
                configuracion['tipocertificado'] = "";
            }
            configuracion['path_certificado'] = "";
            if ($('#actualizar_certificado').val() == 'true') {
                if ($("#certificadofile")[0].files.length > 0) {
                    configuracion['path_certificado'] = $("#certificadofile")[0].files[0].path;
                    configuracion['tipocertificado'] = $("#certificadofile")[0].files[0].name;
                }
            };




            cs.insertar_configuracion(function(data1) {

                if (global.modelo != configuracion['modelo']) {
                    global.isLogged = true;


                    console.log("correctamente logeado");
                    var ud = new UsuarioService();
                    ud.getUsuario(function(data) {
                        global.pais = data.rows.item(0).pais;
                        //alert(global.pais); 
                        global.nombres = data.rows.item(0).nombres;
                        global.idusuario = data.rows.item(0).id;
                        global.showayuda = data.rows.item(0).showayuda;
                        //alert(global.showayuda);
                        ud.updateShowAyuda(function() {}, global.username, 'false');
                        $(".modal-backdrop").remove();
                        load_main();
                        mensaje(data1, 1);
                        mostrarestadolicencia(cs);
                    }, global.username);
                } else {
                    mensaje(data1, 3);
                    mostrarestadolicencia(cs);
                }

            }, configuracion);
        });

    } else {
        mensajeFatcaFacil("El contacto y el email no pueden quedar vac&iacute;os", 3)
    }


}




//Giancarlo .

FatcaController.prototype.getListaEnvio = function() {
    var es = new EnvioService();
    es.getListaEnvio(function(data) {


        for (i = 0; i < data.rows.length; i++) {
            //$("#select_depa").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDIS+ '</option>');
            //console.log(data.rows.item(i));
            $("#listEnvios").append("<table class='table'><tbody><tr><td><img data-toggle='tooltip' data-placement='right' title='Envio fallido' src='imagenes/error.png' width='16' height='16' /></td> " +
                "<td>" + data.rows.item(i).NOMBRE + "</td>" +
                "<td>" + data.rows.item(i).fechacreacion + "<br><strong>12:45 AM.</strong></td>" +
                "<td>" + data.rows.item(i).fechaenvio + "<br><strong>12:45 AM.</strong></td></tr></tbody></table>"
            );
        }

    });
}

FatcaController.prototype.getListaPeriodo = function() {
    var cs = new ConfiguracionService();
    cs.getListaPeriodo(function(data) {
        if (data.rows.length > 0) {
            $('#tipo_periodo').empty();

            for (i = 0; i < data.rows.length; i++) {
                //$("#select_depa").append('<option value=' + data.rows.item(i).CODUBIGEO + '>' + data.rows.item(i).NOMDIS+ '</option>');
                //console.log(data.rows.item(i));href='javascript:pasarVariables('destino.html', 'var1,var2')'    href='#periodos.html'
                console.log(data.rows.item(i));
                $("#listPeriodos").append("<li ><a href='#' onclick='fc.getListaEnvioPorPeriodo(" + data.rows.item(i).ANO + ")'>" + data.rows.item(i).ANO + "<img class='next' src='imagenes/next.png' width='7' height='11'></a></li>");

                $("#tipo_periodo").append("<option value=" + data.rows.item(i).ANO + ">" + data.rows.item(i).ANO + "</option>");
            }
            fc.getListaEnvioPorPeriodo(data.rows.item(i - 1).ANO);
        }
    });
}

FatcaController.prototype.getListaPais = function() {
    var cs = new ConfiguracionService();
    cs.getListaPais(function(data) {
        if (data.rows.length > 0) {
            $('#tipo_periodo').empty();

            for (i = 0; i < data.rows.length; i++) {

                console.log(data.rows.item(i));
                $("#listPeriodos").append("<li ><a href='#' onclick='fc.getListaEnvioPorPeriodo(" + data.rows.item(i).ANO + ")'>" + data.rows.item(i).ANO + "<img class='next' src='imagenes/next.png' width='7' height='11'></a></li>");

                $("#tipo_periodo").append("<option value=" + data.rows.item(i).ANO + ">" + data.rows.item(i).ANO + "</option>");
            }
            fc.getListaEnvioPorPeriodo(data.rows.item(i - 1).ANO);
        }
    });
}


FatcaController.prototype.getListaEnvioPorPeriodo = function(anio) {
    $("#d_informes").html("");

    //$("#downloadfile").change(function() {return false;});

    $("#wrapper").load("html/envios.html", function() {

        var es = new EnvioService();
        es.getListaEnvioPorPeriodo(function(data) {

            var len = data.rows.length,
                i;
            for (i = 0; i < len; i++) {
                //console.log("start data rows")
                //console.log(data.rows.item(i));
                //console.log("end data rows")
                var card_info_active = '';
                var icon = '';
                if (i == 0)
                    card_info_active = 'card_info_active';
                icon = '<td><img data-toggle=tooltip data-placement=right title=Activo src=imagenes/good_hover.png width=16 height=16 /></td>';


                var onclickx = "$(\"#search_text\").val(\"\");fc.getListaCuentaPorPeriodo(" + data.rows.item(i).ID + ",0);$(\"#i_envio_text\").val(" + data.rows.item(i).ID + ")";

                if (i != 0) {
                    icon = '<td><img data-toggle=tooltip data-placement=right title=Inactivo src=imagenes/error.png width=16 height=16 /></td>';
                }


                var identificador = "";
                if (!isNulo(data.rows.item(i).identificador)) {
                    identificador = "<tr>" +
                        "<td>&nbsp;</td>" +
                        "<td>Id:</td>" +
                        "<td>" + data.rows.item(i).identificador + "</td>" +
                        "</tr>"
                }


                var tiporeporte = "";
                if (!isNulo(data.rows.item(i).tiporeporte)) {
                    tiporeporte = "<tr>" +
                        "<td>&nbsp;</td>" +
                        "<td>T. Reporte:</td>" +
                        "<td>" + data.rows.item(i).tiporeporte + "</td>" +
                        "</tr>"
                }

                $("#listPeriodosPorAnio").append("<div class='card_info " + card_info_active + "' id='card_1'onclick='" + onclickx + "'><table class='table'>" +
                    "<tbody style='text-align: left;''>" +
                    "<tr>"

                    +
                    icon

                    +
                    "<td>" +
                    "Archivo:" +
                    "</td>"
                    //+"<td>"+data.rows.item(i).ID+"</td>"
                    +
                    "<td>" + data.rows.item(i).archivogenerado + "</td>"
                    /*\n\n"+data.rows.item(i).identificador+"</td>"
                                                +"<td>"+data.rows.item(i).fechacreacion+"</td>"
                                                +"<input id='informe_envio_"+data.rows.item(i).ID+"' type='hidden' value= '"+data.rows.item(i).informe +"'/>"*/
                    +
                    "</tr>" +
                    "<tr>" +
                    "<td>&nbsp;</td>" +
                    "<td>Hora:</td>" +
                    "<td>" + data.rows.item(i).fechacreacion + "</td>" +
                    "</tr>" +
                    identificador +
                    tiporeporte

                    +



                    "</tbody>" +
                    "</table>" +
                    "<input id='informe_envio_" + data.rows.item(i).ID + "' type='hidden' value= '" + data.rows.item(i).informe + "'/>" +
                    "<input id='informe_nulo_" + data.rows.item(i).ID + "' type='hidden' value= '" + data.rows.item(i).reportenulo + "'/></div>"

                  
                );

                //Usar para notificaccccccciones antes de tbody
                 /*                   "<tr>"+
                    "<td></td>"+
                    "<td colspan=2>"+
                    "<a class='abrir_notifi badge'  onclick='abrirNotificacion(arrow_"+ data.rows.item(i).ID+")' data-toggle='collapse' href='#collapse_"+ data.rows.item(i).ID+"' data-noti=1>Notificaciones <i id='arrow_"+data.rows.item(i).ID+"' class='fa fa-angle-down' aria-hidden=true></i>"+
                    "</a>"+
                    "</td>"+
                    "</tr>"+*/

                //Agregar al final, 
                /*  "<div id='collapse_"+data.rows.item(i).ID+"' class='panel-collapse collapse notifi-bloque'>"+
                    "<div id='inner-content-div4'>"+
                    "<dl>"+
                    "<dt>Mensaje:</dt>"+
                    "<dd>El archivo no se puedo subir a la plataforma</dd>"+
                    "</dl>"+
                    "<hr>"+
                    "<dl>"+
                    "<dt>Observaciones:</dt>"+
                    "<dd>Error en el tag AddressName - Sponsor</dd>"+
                    "<dd>Error en el tag Name</dd>"+
                    "</dl>"+
                    "</div>"+
                    "</div>"*/


            }


            if (len == 0) {
                $("#b_descargarXML").prop("disabled", true);
                $("#b_descargarModelo").prop("disabled", true);
            } else {

                $("#b_descargarXML").click(function() {
                    fc.opendownloadFile(anio)
                });
                if (data.rows.item(0).packagezip != "" && data.rows.item(0).packagezip != "null" && data.rows.item(0).packagezip != undefined) {
                    $("#b_descargarModelo").click(function() {
                        fc.opendownloadpackageFile(anio)
                    });
                } else {
                    $("#b_descargarModelo").hide();
                }
            }
            //$("#open_poolreporte").click(function(){open_poolreporte(anio)}); 


            $(".wra_path_2").html("Historial del periodo: <b>" + anio + "</b>");

            var i = 0;
            var ant = this;
            $('#wra_path_3 #card_1').click(function() {

                if ($(ant).hasClass("card_select")) {
                    $(ant).removeClass("card_select");
                    var target = $(ant).attr('href');
                    $(target).toggleClass('hidden show');

                }
                if ($(this).hasClass("card_select")) {

                    $(this).removeClass("card_select");
                    var target = $(this).attr('href');
                    $(target).toggleClass('hidden show');
                } else {
                    $(this).addClass("card_select");
                    var target = $(this).attr('href');
                    $(target).toggleClass('hidden show');
                }

                ant = this;
            });

        }, anio)
    });
}


FatcaController.prototype.getListaCuentaPorPeriodo = function(idenvio, pagina) {


    var t_var = $("#informe_envio_" + idenvio).val();
    var t_var2 = $("#informe_nulo_" + idenvio).val(); //GVR 16-02-2017
    //alert(t_var);
    if ((t_var != 'null' && t_var != '[]' & t_var != undefined && t_var != "") || (t_var2 != 'null' && t_var2 != '[]' & t_var2 != undefined && t_var2 != "")) {
        $("#d_informes").html('<button type="button" class="btn btn_fatca " id="b_informes" onclick="open_poolreporte(' + idenvio + ')">Ver Reporte</button>');
    } else {
        $("#d_informes").html('');
    }

    var es = new EnvioService();

    es.getTotalCuentaPorEnvio(function(data1) {
        var total_resultado = data1.rows.item(0).count;
        var total_pag = parseInt(data1.rows.item(0).count / 10);

        if (total_resultado - parseInt(data1.rows.item(0).count / 10) * 10 > 0) {
            total_pag = total_pag + 1;
        }

        $("#b_next_pag").off();
        $("#b_previous_pag").off();



        if (pagina > 0) {
            $("#b_previous_pag").click(function() {
                fc.getListaCuentaPorPeriodo(idenvio, pagina - 1)
            });
            $("#b_previous_pag").prop("disabled", false);
        } else {
            $("#b_previous_pag").prop("disabled", true);
        }

        if (pagina + 1 < total_pag) {
            $("#b_next_pag").click(function() {
                fc.getListaCuentaPorPeriodo(idenvio, pagina + 1)
            });
            $("#b_next_pag").prop("disabled", false);
        } else {
            $("#b_next_pag").prop("disabled", true);
        }




        if (total_resultado > 0) {
            $("#l_resultados").html("<b>" + total_resultado + " resultados </b> (P&aacute;gina " + (pagina + 1) + " de " + total_pag + ")");
        } else {
            $("#l_resultados").html("<b>" + total_resultado + " resultados </b>");
        }
        es.getListaCuentaPorPeriodo(function(data) {
            $("#listCuentaPorPeriodo").empty();
            var len = data.rows.length,
                i;
            for (i = 0; i < len; i++) {
                console.log(data.rows.item(i));

                $("#listCuentaPorPeriodo").append("<tr onclick=getDetalles(" + data.rows.item(i).idenvio + "," + data.rows.item(i).ID + ");>" +
                    "<td>" + data.rows.item(i).propietario + "" +
                    "<td>" + data.rows.item(i).numerocuenta + "</td>" +
                    "<td>" + data.rows.item(i).monto + "</td>" +
                    "<td>" + data.rows.item(i).tipomoneda + "</td></tr>");
            }


        }, idenvio, pagina, $("#search_text").val());

    }, idenvio, $("#search_text").val());


    //});
}



FatcaController.prototype.ocseJson = function() { //(Subir archivos csv ,excel,vacio) 

    try {
        fs.unlinkSync(gui.App.dataPath + "\\Files" + "\\stdout.json");
    } catch (err) {

    }

    var cs = new ConfiguracionService();
    var es = new EnvioService();
    var tipolicencia = "trial";

    var execPath = path.dirname(process.execPath);

    cs.ocseJson(function(data, msg) {


        mensaje_loading("");
        if ( (($("#archivo")[0].files.length > 0 && $('#tipo_archivo').val() != 'VACIO') || $('#tipo_archivo').val() == 'VACIO') && $('#tipo_periodo').val() != 'VACIO' )  {

                if ($('#tipo_archivo').val() != 'VACIO') {
                    data.archivo = $("#archivo")[0].files[0].path;
                } else {
                    data['archivo'] = "";
                    data['tipoReporte'] = $('#i_tipofatca').val();
                    data['identificador'] = $('#i_identificadorenvio').val();
                }


                data.periodo = $('#tipo_periodo').val();
                data['tipoArchivo'] = $('#tipo_archivo').val();


                var envio_licencia = "trial";
                var envio_versionfatca = gui.App.manifest.version;

                data['installPath'] = execPath;

                if (data['licencia'] == 'trial') {

                } else {
                    tipolicencia = "";
                    envio_licencia = data.entidad.giin;

                }


                console.log("======>");
                console.log(JSON.stringify(data));
                ///////////////
                outputFilename = gui.App.dataPath + "\\Files" + "\\json.json"
                fs.writeFile(outputFilename, JSON.stringify(data, null, 4), function(err) {
                    if (err) {
                        console.log(err);
                        //mensaje_loading("");
                    } else {

                        $('#upload_dialog').modal('toggle');
                        ///////////////CQ
                        var path = require('path');
                        var path_java = '"' + path.dirname(process.execPath) + "/Runtime/bin/java" + '"';
                        var java_lib = '"' + path.dirname(process.execPath) + "/Runtime/lib/jff.jar " + '" ';
                        var parameters = " -jar " + java_lib + ' -p "' + outputFilename + '"';



                        ///////Solo para MAC ////////
                        //java_home = "";
                        //parameters = " -jar /Users/ceqs/Desarrollos/Eclipse/Java/complexless-fatca-util-v1/build/jff.jar " + outputFilename;
                        /////////////////////////////

                        console.log(path_java);
                        var cmd = path_java + parameters;
                        console.log(cmd);

                        var exec = require('child_process').exec,
                            child;
                        child = exec(cmd,
                            function(error, stdout, stderr) {
                                mensaje_loading("");
                                console.log('stdout: ' + stdout);
                                console.log('stderr: ' + stderr);

                                console.log(error);
                                if (error !== null) {

                                    mensajeFatcaFacil("Ocurri&oacute; un error desconocido", 4)
                                } else {

                                    var res = jQuery.parseJSON(stdout);
                                    //var res = jQuery.parseJSON('{"codigoError":0,"jsonFilePath":"C:\\Users\\Admin\\AppData\\Local\\FatcaFacil\\Files\\\\stdout.json"}')
                                    //                          {"codigoError":0,"jsonFilePath":"C:\\Users\\Admin\\AppData\\Local\\FatcaFacil\\Files\\\\stdout.json"}

                                    console.log("aqui el var");
                                    console.log(res);
                                    console.log(res.codigoError);
                                    if (res.codigoError == 0) {


                                        console.log(stdout);


                                        //var pubkey = publicPem.toString();    
                                        var jJsonObj = {};
                                        try {
                                            jJsonObj = jQuery.parseJSON(fs.readFileSync(gui.App.dataPath + "\\Files" + "\\stdout.json").toString());
                                        } catch (err) {
                                            jJsonObj = jQuery.parseJSON('{"codigoError":"1","mensajeError":"Al archivo no se puede leer"}');

                                        }

                                        //jJsonObj={};
                                        //      jJsonObj['codigoError']=0;
                                        //      jJsonObj['mensajeError']="";
                                        //      jJsonObj['archivoZip']="file.zip";
                                        //      jJsonObj['nombreArchivo']="";
                                        //      jJsonObj['fechaGenera']="";
                                        //      jJsonObj['cuentas']=[];
                                        //      jJsonObj['cuentas'][0]={};
                                        //      jJsonObj['cuentas'][0]['nombre']='Carlos Quispe';
                                        //      jJsonObj['cuentas'][0]['numeroCuenta']='1234566';
                                        //      jJsonObj['cuentas'][0]['balance']='100';
                                        //      jJsonObj['cuentas'][0]['tipoMoneda']='soles';   
                                        //      console.log(JSON.stringify(jJsonObj));

                                        //if(jJsonObj.codigoError==0){

                                        es.InsertEnvio(function(m) {
                                            console.log("el objeto es :");
                                            console.log(jJsonObj);
                                            if (tipolicencia == "") {
                                                mensajeFatcaFacil(m, 1);
                                            } else {
                                                mensajeFatcaFacil(m + " Se ha utilizado una licencia trial. Para mayor informaci&oacute;n visite <a href='#' onclick='gui.Shell.openExternal(\"http://fatcafacil.com/\");'>www.fatcafacil.com</a>", 2);
                                            }
                                            fc.getListaEnvioPorPeriodo($('#tipo_periodo').val());
                                        }, $('#tipo_periodo').val(), '', jJsonObj, $('#tipo_archivo').val(), envio_versionfatca, envio_licencia);
                                        //}else{
                                        //  mensaje(jJsonObj.mensajeError);
                                        //} // $('#upload_dialog').modal('toggle'); 
                                    } else {
                                        mensajeFatcaFacil("Ocurri&oacute; un error: " + res.mensajeError, 4);
                                    }

                                }

                            });



                    }
                }); 

        }else {
            mensaje_loading("");
            mensajeFatcaFacil("Debe seleccionar un archivo o un periodo", 3);

        }
     
       
     
    });

}





FatcaController.prototype.ocseJsonNotif = function() { // (Subir notificacion)

    try {
        fs.unlinkSync(gui.App.dataPath + "\\Files" + "\\stdout.json");
    } catch (err) {

    }

    var cs = new ConfiguracionService();
    var es = new EnvioService();
    var tipolicencia = "trial";

    var execPath = path.dirname(process.execPath);

    cs.ocseJson(function(data, msg) {

        mensaje_loading("");

        if ( $("#archivo_notificacion")[0].files.length > 0  )  {
                 data.archivo = $("#archivo_notificacion")[0].files[0].path;
                 console.log("el objeto json - notificacion es ");
          
                 outputFilename = gui.App.dataPath + "\\Files" + "\\json.json"
                 fs.writeFile(outputFilename, JSON.stringify(data, null, 4), function(err) {
                            if (err) {
                                console.log(err);
                                //mensaje_loading("");
                            } else {


                                    console.log("el objeto json - notificacion es ");
                      

                                      $('#upload_dialog_notificacion').modal('toggle');
                                        ///////////////CQ
                                        var path = require('path');
                                        var path_java = '"' + path.dirname(process.execPath) + "/Runtime/bin/java" + '"';
                                        var java_lib = '"' + path.dirname(process.execPath) + "/Runtime/lib/jff.jar " + '" ';
                                        var parameters = " -jar " + java_lib + ' -u "' + outputFilename + '"';


                                        console.log(path_java);
                                        var cmd = path_java + parameters;
                                        console.log(cmd);

                                        var exec = require('child_process').exec,
                                            child;
                                        child = exec(cmd,
                                            function(error, stdout, stderr) {
                                                mensaje_loading("");
                                                console.log('stdout: ' + stdout);
                                                console.log('stderr: ' + stderr);

                                                console.log(error);
                                                if (error !== null) {

                                                   mensajeFatcaFacil("Ocurri&oacute; un error desconocido", 4)
                                                   
                                                } else {

                                                    var res = jQuery.parseJSON(stdout);
                                                    console.log("aqui el var");
                                                    console.log(res);
                                                    console.log(res.codigoError);
                                                    if (res.codigoError == 0) {

                                                         var jJsonObj = {};
                                                            try {
                                                                jJsonObj = jQuery.parseJSON(fs.readFileSync(gui.App.dataPath + "\\Files" + "\\stdout.json").toString());

                                                                 $('#show_dialog_notificacion').modal('toggle');
                                                                 console.log(jJsonObj);
                                                                 console.log(jJsonObj.fechaHora);
                                                                 var fecha_hora =  jJsonObj.fechaHora;
                                                                 var identif = jJsonObj.identificador;
                                                                 var mensaje = jJsonObj.mensaje;
                                                                 var accion = jJsonObj.accion;
                                                                 if(fecha_hora === undefined){
                                                                    fecha_hora = "Ninguno";
                                                                 }
                                                                 if(identif === undefined){
                                                                    identif = "Ninguno";
                                                                 }
                                                                 if(mensaje === undefined){
                                                                    mensaje = "Ninguno";
                                                                 }
                                                                 if(accion === undefined){
                                                                    accion = "Ninguno";
                                                                 }

                                                                 

                                                                 //0 : sin error
                                                                 //1 : error
                                                                 //2 : OK - Observaciones

                               
                                                                 if(jJsonObj.tipo == "0"){// OK

                                                                     $("#notificacion_container").html('<div class="alert alert-danger alert-notification alert-notification_correcto" role="alert">'+
                                                                     '<span id"icono_notificacion" class="glyphicon glyphicon-ok alert-notification_icon alert-correcto_icon"></span>'+
                                                                     '<dl style="margin:0;padding-left:15px; display:inline-block">'+
                                                                     '<dt><strong style="font-size: 1.5rem;">Validado</strong></dt>'+
                                                                     '<dd style="font-size: 1.5rem;">Su documento fue validado.</dd>'+
                                                                     '</dl></div>');

                                                                 }

                                                                 if(jJsonObj.tipo == "1"){//ERROR

                                                                     $("#notificacion_container").html('<div class="alert alert-danger alert-notification alert-notification_error" role="alert">'+
                                                                     '<span id"icono_notificacion" class="glyphicon glyphicon-remove alert-notification_icon alert-error_icon"></span>'+
                                                                     '<dl style="margin:0;padding-left:15px; display:inline-block">'+
                                                                     '<dt><strong style="font-size: 1.5rem;">Error</strong></dt>'+
                                                                     '<dd style="font-size: 1.5rem;">Su documento tiene errores.</dd>'+
                                                                     '</dl></div>');

                                                                 }

                                                                 if(jJsonObj.tipo == "2"){//OBSERVACIONES

                                                                     $("#notificacion_container").html('<div class="alert alert-danger alert-notification alert-notification_observacion" role="alert">'+
                                                                     '<span id"icono_notificacion" class="glyphicon glyphicon-alert alert-notification_icon alert-observacion_icon"></span>'+
                                                                     '<dl style="margin:0;padding-left:15px; display:inline-block">'+
                                                                     '<dt><strong style="font-size: 1.5rem;">Observaciones</strong></dt>'+
                                                                     '<dd style="font-size: 1.5rem;">Su documento es valido , pero tiene observaciones.</dd>'+
                                                                     '</dl></div>');

                                                                 }


                                                                 $('#fecha_hora_notificacion').val(fecha_hora);
                                                                 $('#identificador_notificacion').val(identif);
                                                                 $('#mensaje_notificacion').val(mensaje);
                                                                 $('#accion_notificacion').val(accion);
                                                                 
                                                                 mensajeFatcaFacil("Archivo de notificacion subido correctamente ", 1);
                                                            } catch (err) {
                                                                jJsonObj = jQuery.parseJSON('{"codigoError":"1","mensajeError":"Al archivo no se puede leer"}');

                                                            }
                                                    } else {
                                                        mensajeFatcaFacil("Ocurri&oacute; un error: " + res.mensajeError, 4);
                                                       
                                                    }

                                                }

                                            });

                            
                            }
                 });


        } 
        else{
             mensaje_loading("");
             mensajeFatcaFacil("Debe seleccionar un archivo ", 3);
        }
        
    });

}


FatcaController.prototype.getTotalUsuario = function(iusuario) {

    var ud = new UsuarioService();
    ud.getTotalUsuario(function(data) {
        if (data === true) {
            //global.isLogged=true;

            //alert("existe registro");
            window.location.replace("login.html");
        } else {
            //alert("no existe ningun usuario");
        }
    });
}



FatcaController.prototype.opendownloadFile = function(periodo) {

    //$("#downloadfile").val('');
    //$("#downloadfile").change();
    $("#downloadfilecontent").html('<input type="file" id="downloadfile" style="display:none"/>');

    var es = new EnvioService();
    es.getListaEnvioPorPeriodo(function(data) {
        var d = new Date();
        var anio = d.getFullYear();
        //alert(n);
        var len = data.rows.length,
            i;
        if (len > 0) {
            //alert(data.rows.item(0).archivogenerado);
            $("#downloadfile").prop("nwsaveas", data.rows.item(0).archivogenerado);
            $("#downloadfile").click();
            $("#downloadfile").change(function() {
                if ($("#downloadfile")[0].files.length > 0) {
                    fs.createReadStream(data.rows.item(0).archivo).pipe(fs.createWriteStream($("#downloadfile")[0].files[0].path));
                }

            });

        } else {
            mensajeFatcaFacil("No tiene env&iacute;os para descargar", 2);
        }


    }, periodo);


}




FatcaController.prototype.opendownloadpackageFile = function(periodo) {
    //$("#downloadfile").val('');
    //$("#downloadfile").change();
    $("#downloadfilecontent").html('<input type="file" id="downloadfile" style="display:none"/>');
    var es = new EnvioService();
    es.getListaEnvioPorPeriodo(function(data) {
        var d = new Date();
        var anio = d.getFullYear();
        //alert(n);
        var len = data.rows.length,
            i;
        if (len > 0) {
            // alert(data.rows.item(0).packagezip)
            if (data.rows.item(0).packagezip != "" && data.rows.item(0).packagezip != "null" && data.rows.item(0).packagezip != undefined) {
                $("#downloadfile").prop("nwsaveas", data.rows.item(0).packagezip);
                $("#downloadfile").click();
                $("#downloadfile").change(function() {
                    if ($("#downloadfile")[0].files.length > 0) {
                        fs.createReadStream(data.rows.item(0).path_packagezip).pipe(fs.createWriteStream($("#downloadfile")[0].files[0].path));
                    }

                });
            } else {
                mensajeFatcaFacil("No se ha encontrado archivo de modelo para descargar", 2);
            }
        } else {
            mensajeFatcaFacil("No tiene env&iacute;os para descargar", 2);
        }


    }, periodo);


}


FatcaController.prototype.getListaCuentaBusqueda = function(filtro) {

    var es = new EnvioService();
    es.getListaCuentaBusqueda(function(results) {

        console.log("/////////");
        var len = results.rows.length,
            i;
        for (i = 0; i < len; i++) {
            console.log(results.rows.item(i));
        }
        console.log("/////////");
    }, filtro);
}



FatcaController.prototype.checkUpdates = function(filtro) {

    //$.getJSON( "http://192.168.1.7:88/update.php", function( data ) {
    $.getJSON("http://fatcafacil.com/update.json", function(data) {
            //gui.Shell.openExternal("http://www.google.com");
            var v_servidor = data.version.toString();
            var v_app = gui.App.manifest.version.toString();

            if (compararVersiones(v_servidor, v_app) == 1) {
                //alert("retorno 1 :v");


                //if(data.version>gui.App.manifest.version){
                //alert("version nueva");
                $("#updatecontent").html(data.descripcion.content)

                $("#update_link").html("Descargar la versi&oacute;n " + data.version);
                $("#update_link").click(function() {
                    gui.Shell.openExternal(data.files.win);
                });
                console.log(data);


                $("#cookies").addClass("display");
                $("#close-cookies").click(function() {
                    event.preventDefault();
                    $("#cookies").addClass("close-cookies");
                });


            }
        })
        .fail(function() { /*mensaje("Fatcaf&aacute;cil no se puede conectar al servidor para verificar actualizaciones") */ });




    /*
    $.getJSON("example.json", function() {
      alert("success");
    })
    .done(function() { alert('getJSON request succeeded!'); })
    .fail(function() { alert('getJSON request failed! '); })

    */


}


// Return 1 if a > b
// Return -1 if a < b
// Return 0 if a == b
function compararVersiones(a, b) {
    // a = version_servidor
    // b = version_aplicacion
    if (a === b) {
        return 0;
    }

    var a_components = a.split(".");
    var b_components = b.split(".");

    var len = Math.min(a_components.length, b_components.length);

    // bluce mientras components son iguales 
    for (var i = 0; i < len; i++) {
        // A mayor que B
        if (parseInt(a_components[i]) > parseInt(b_components[i])) {
            return 1;
        }

        // B mayor que A
        if (parseInt(a_components[i]) < parseInt(b_components[i])) {
            return -1;
        }
    }

    // 
    if (a_components.length > b_components.length) {
        return 1;
    }

    if (a_components.length < b_components.length) {
        return -1;
    }

    // Si no , son iguales.
    return 0;
}


//Funcion mensaje

function mensajeFatcaFacil(mensaje, tipo) {

    var tipemessage = "";
    if (tipo === 1) {
        tipemessage = "success";
    } else if (tipo === 2) {
        tipemessage = "info";
    } else if (tipo === 3) {
        tipemessage = "warning";
    } else if (tipo === 4) {
        tipemessage = "danger";
    } else {
        tipo = 1;
        tipemessage = "success";
    }


    //$.fn.dpToast(mensaje,2000);
    $.toaster.reset();
    if (tipo === 1) {
        $.toaster({
            settings: {
                toast: {
                    display: function($toast) {
                        return $toast.fadeIn('slow');
                    },
                    remove: function($toast, callback) {
                        return $toast.animate({
                            opacity: '0',
                            height: '0px'
                        }, {
                            duration: 'slow',
                            complete: callback
                        });
                    },
                },
                timeout: 3000,
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil',
            message: mensaje
        });
    }
    if (tipo === 2) {
        $.toaster({
            settings: {
                toast: {
                    display: function($toast) {
                        return $toast.fadeIn('slow');
                    },
                    remove: function($toast, callback) {
                        return $toast.animate({
                            opacity: '0',
                            height: '0px'
                        }, {
                            duration: 'slow',
                            complete: callback
                        });
                    },
                },
                timeout: 4000,
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil - Info',
            message: mensaje
        });
    }
    if (tipo === 3) {
        $.toaster({
            settings: {
                toast: {
                    display: function($toast) {
                        return $toast.fadeIn('slow');
                    },
                    remove: function($toast, callback) {
                        return $toast.animate({
                            opacity: '0',
                            height: '0px'
                        }, {
                            duration: 'slow',
                            complete: callback
                        });
                    },
                },
                timeout: 4000,
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil - Aviso',
            message: mensaje
        });
    }

    if (tipo === 4) {
        $.toaster({
            settings: {
                toast: {
                    fade: { in: 'fast',
                        out: 'slow'
                    },


                    remove: function($toast, callback) {
                        /*alert("remove");
                          return $toast.animate(
                          {
                              opacity: '0',
                              height: '0px'
                          },
                          {
                              duration: settings.toast.fade.out,
                              complete: callback
                          });*/
                    }
                }
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil - ERROR',
            message: mensaje
        });
    }


}
