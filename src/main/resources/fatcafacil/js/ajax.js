$.navAsAjax = true;


if ($.navAsAjax) {
    //alert("iicio2");
    // fire this on page load if nav exists
    if ($('nav').length) {
        checkURL();
        //alert("iicio");
    };

    $(document).on('click', 'nav a[href!="#"]', function(e) {
        e.preventDefault();
        var $this = $(e.currentTarget);

        // if parent is not active then get hash, or else page is assumed to be loaded
        //alert("tocaron");
        //checkURL();
        if (!$this.parent().hasClass("active") && !$this.attr('target')) {

            //alert("tocaron2");
            if (window.location.search) {
                window.location.href =
                    window.location.href.replace(window.location.search, '')
                    .replace(window.location.hash, '') + '#' + $this.attr('href');
                // checkURL();
                //alert("entero1");
            } else {
                window.location.hash = $this.attr('href');
                //alert("entro 2");
                checkURL();
            }
            ///}
        }

    });
}

// CHECK TO SEE IF URL EXISTS
function checkURL() {

    //get the url by removing the hash
    var url = location.hash.replace(/^#/, '');
    //alert("cheked urlk");
    container = $('#wrapper');
    // Do this if url exists (for page refresh, etc...)
    if (url) {
        // remove all active class
        //alert("entre1"+url);
        $('nav li.active').removeClass("active");
        // match the url and add the active class
        $('nav li:has(a[href="' + url + '"])').addClass("active");
        var title = ($('nav a[href="' + url + '"]').attr('title'))

        // change page title from global var
        document.title = (title || document.title);
        //console.log("page title: " + document.title);

        // parse url to jquery
        loadURL(url + location.search, container);
    } else {
        // alert("url");
        // grab the first URL from nav
        var $this = $('nav > ul > li:first-child > a[href!="#"]');

        //update hash
        // window.location.hash = "#comprobantes.html";///$this.attr('href');

        loadURL("periodos.html" + location.search, container);

    }

}

// LOAD AJAX PAGES

function loadURL(url, container) {
    //console.log(container)

    $.ajax({
        type: "GET",
        url: url,
        dataType: 'html',
        cache: true, // (warning: this will cause a timestamp and will call the request twice)
        beforeSend: function() {
            // cog placed
            // alert(url);

            container.html('<h1><i class="fa fa-cog fa-spin"></i> Loading...</h1>');

            // Only draw breadcrumb if it is main content material
            // TODO: see the framerate for the animation in touch devices

            if (container[0] == $("#wrapper")[0]) {
                // scroll up
                $("html").animate({
                    scrollTop: 0
                }, "fast");
            }
        },
        /*complete: function(){
            // Handle the complete event
            // alert("complete")
        },*/
        success: function(data) {
            // cog replaced here...

            container.css({
                opacity: '0.0'
            }).html(data).delay(50).animate({
                opacity: '1.0'
            }, 300);


        },
        error: function(xhr, ajaxOptions, thrownError) {

            container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
        },
        async: false

    });
}