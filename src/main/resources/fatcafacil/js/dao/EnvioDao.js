//var SELECT_ENVIO_PERIODO= "select id,nombre,fechacreacion,fechaenvio,tipoarchivo,archivo,archivogenerado,packagezip,path_packagezip,informe,identificador,tiporeporte from ENVIOS where ANO=? and idusuario=? ORDER BY id desc";
//Nuevo campo reportenulo;
var SELECT_ENVIO_PERIODO = "select id,nombre,fechacreacion,fechaenvio,tipoarchivo,archivo,archivogenerado,packagezip,path_packagezip,informe,identificador,tiporeporte,reportenulo from ENVIOS where ANO=? and idusuario=? ORDER BY id desc";

var SELECT_CUENTA_PERIODO = "select ID,idenvio,propietario,numerocuenta,monto,tipomoneda from CUENTA where idenvio=? order by id  asc LIMIT ? OFFSET ?";
var SELECT_CUENTA_PERIODO_FILTRO = "select propietario,numerocuenta,monto,tipomoneda from CUENTA where idenvio=? AND (propietario like ? OR numerocuenta like ?) order by id  asc LIMIT ? OFFSET ? ";
var SELECT_TOTAL_CUENTA = "select count(1) count from CUENTA where idenvio=?";
var SELECT_TOTAL_CUENTA_FILTRO = "select count(1) count from CUENTA where idenvio=? AND (propietario like ? OR numerocuenta like ?) ";

var SELECT_CUENTA_DETALLE = "select informacion from CUENTA where idenvio=? AND ID=?";

//var INSERT_ENVIO="INSERT INTO ENVIOS(idusuario,ANO,fechacreacion,archivo,tipoarchivo,archivogenerado,packagezip,path_packagezip,informe,versionfatca,licencia,identificador,tiporeporte) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);";
//Nueva colimna agregado. GVR 16-02-2017
var INSERT_ENVIO = "INSERT INTO ENVIOS(idusuario,ANO,fechacreacion,archivo,tipoarchivo,archivogenerado,packagezip,path_packagezip,informe,versionfatca,licencia,identificador,tiporeporte,reportenulo) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

var INSERT_CUENTA = "INSERT INTO CUENTA(IDENVIO,propietario,numerocuenta,monto,tipomoneda,informacion) VALUES(?,?,?,?,?,?);";
var SELECT_CUENTA_BUSQUEDA = "select * from CUENTA WHERE propietario like ? OR numerocuenta like ?";




EnvioDao = Backbone.Model.extend({
    initialize: function() {
        console.log('EnvioDao');

        this.bind('invalid', function(model, error) {
            console.error(error);
        });
    },
    getListaEnvioPorPeriodo: function(callback, anio, idusuario) {
        db.transaction(function(tx, results) {
            tx.executeSql(SELECT_ENVIO_PERIODO, [anio, idusuario], function(tx, results) {

                /*var total=0;
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                 total= results.rows.item(i);
                }*/
                //console.log()
                //console.log(total);
                /*var len = results.rows.length, i;
                console.log(len);
                console.log(results);*/
                callback(results /*,results.rows.item(0).ID*/ );

            });
        });
    },
    getListaCuentaPorPeriodo: function(callback, idenvio, limit, offset, filtro) {
        db.transaction(function(tx) {
            if (filtro == "") {
                tx.executeSql(SELECT_CUENTA_PERIODO, [idenvio, limit, offset], function(tx, results) {


                    callback(results);

                });
            } else {

                filtro = '%' + filtro + '%';
                tx.executeSql(SELECT_CUENTA_PERIODO_FILTRO, [idenvio, filtro, filtro, limit, offset], function(tx, results) {


                    callback(results);

                });
            }

        });
    },
    getCuentaDetalle: function(callback, idenvio, id) {
        db.transaction(function(tx) {


            tx.executeSql(SELECT_CUENTA_DETALLE, [idenvio, id], function(tx, results) {

                callback(results);

            });

        });
    },
    getTotalCuentaPorEnvio: function(callback, idenvio, filtro) {
        db.transaction(function(tx) {
            if (filtro == "") {
                tx.executeSql(SELECT_TOTAL_CUENTA, [idenvio], function(tx, results) {


                    callback(results);

                });
            } else {

                filtro = '%' + filtro + '%';
                tx.executeSql(SELECT_TOTAL_CUENTA_FILTRO, [idenvio, filtro, filtro], function(tx, results) {


                    callback(results);

                });
            }

        });


    },
    InsertEnvio: function(callback, idusuario, ANO, fechacreacion, jJsonObj, tipoarchivo, versionfatca, licencia) {

        console.log("----->" + ANO);
        console.log("----->" + fechacreacion);
        console.log(jJsonObj);
        console.log(JSON.stringify(jJsonObj.informes));
        console.log(JSON.stringify(jJsonObj.reporte));
        console.log("----->" + tipoarchivo);

        var packageZip = "";
        var packagePath = "";
        var reporte_nulo = "";

        if (jJsonObj.packageZip != undefined) {
            packageZip = jJsonObj.packageZip;
        }

        if (jJsonObj.packagePath != undefined) {
            packagePath = jJsonObj.packagePath;
        }
        if (jJsonObj.reporte != undefined) {
            reporte_nulo = JSON.stringify(jJsonObj.reporte);
        }

        db.transaction(function(tx) {

            tx.executeSql(INSERT_ENVIO, [
                idusuario,
                ANO,
                getDateTime(),
                jJsonObj.archivoZip,
                tipoarchivo,
                jJsonObj.nombreArchivo,
                packageZip,
                packagePath,
                JSON.stringify(jJsonObj.informes), versionfatca, licencia,
                jJsonObj.mensaje.identificador,
                jJsonObj.mensaje.tipoReporte,
                reporte_nulo
            ], function(tx, results) {
                //jJsonObj
                for (i = 0; i < jJsonObj.cuentas.length; i++) {

                    tx.executeSql(INSERT_CUENTA, [
                        results.insertId,
                        jJsonObj.cuentas[i].nombre,
                        jJsonObj.cuentas[i].numeroCuenta,
                        jJsonObj.cuentas[i].balance,
                        jJsonObj.cuentas[i].tipoMoneda,
                        JSON.stringify(jJsonObj.cuentas[i])
                    ]);
                }
                //console.log(results);

                callback("Archivo generado correctamente.");

            });
        });
    },
    getListaCuentaBusqueda: function(callback, filtro) {
        db.transaction(function(tx) {

            tx.executeSql(SELECT_CUENTA_BUSQUEDA, [propietario, numerocuenta], function(tx, results) {

                callback(results);

            });
        });
    }
    /* InsertCuenta:function (callback,IDENVIO,propietario,numerocuenta,monto,tipomoneda){
         db.transaction(function (tx) {
           tx.executeSql(INSERT_CUENTA, [IDENVIO,propietario,numerocuenta,monto,tipomoneda], function (tx, results) {

             callback(results);

           });
         });
     }*/


    /*getListaEnvio:function (callback){
                db.transaction(function (tx) {
                    tx.executeSql(SELECT_ENVIO, [], function (tx, results) {
                   // var len = results.rows.length, i;
                   // var result = [];
                   // for (i = 0; i < len; i++) {
                   //   result[i] = results.rows.item(i);
                   // }
                    callback(results); //result
                  });
                });
            }*/
});