var SELECT_TOTAL = "select count(1) total   from users ";
var SELECT_USER = "select count(1) total   from users WHERE username=? and  password=? ";
var INSERT_USER = "Insert into users (username, password, nombres, apellidos) values (?,?,?,?)";
var INSERT_USER_CONFIGURACION = "INSERT OR REPLACE INTO CONFIGURACION(idusuario , pais,modelo,contacto,emailcontacto) VALUES (?,?,?,?,?)";
var UPDATE_PASS_USER = "update users set password=? where username=?";
var SELECT_PASS_USER = "select password from users WHERE username=? and password=?"
var SELECT_NAME_USER = "select id,username,password,nombres nombre from users WHERE username=?";
var SELECT_NAME_USER2 = "select u.id,u.username,u.password,u.nombres||' '||u.apellidos nombres,c.pais,u.showayuda from USERS u join CONFIGURACION c on u.id=c.idusuario  WHERE u.username=? ";
var UPDATE_SHOWAYUDA = "UPDATE USERS SET showayuda= ? where username = ?";

UsuarioDao = Backbone.Model.extend({
    initialize: function() {
        console.log('hello world');

        this.bind('invalid', function(model, error) {
            console.error(error);
        });
    },
    getTotal: function(callback) {
        db.transaction(function(tx) {
            tx.executeSql(SELECT_TOTAL, [], function(tx, results) {
                var total = 0;
                var len = results.rows.length,
                    i;
                for (i = 0; i < len; i++) {
                    total = results.rows.item(i);
                }
                callback(total);
            });
        });
    },
    setUsuario: function(callback, iusuario, ipassword, inombre, iapellidos, ipais, imodelo, contacto, emailcontacto) {
        db.transaction(function(tx) {
            tx.executeSql(INSERT_USER, [iusuario, ipassword, inombre, iapellidos], function(tx, results) {
                console.log(results);
                tx.executeSql(INSERT_USER_CONFIGURACION, [results.insertId, ipais, imodelo, contacto, emailcontacto], function(tx, results) {});
                callback(results);
            })




        });
    },
    checkUsuario: function(callback, iusuario, ipassoword) {
        db.transaction(function(tx) {
            tx.executeSql(SELECT_USER, [iusuario, ipassoword], function(tx, results) {

                var len = results.rows.item(0).total;

                if (len > 0) {
                    callback(true);
                } else
                    callback(false);

            });
        });
    },
    getTotalUsuario: function(callback) {
        db.transaction(function(tx) {
            tx.executeSql(SELECT_TOTAL, [], function(tx, results) {

                var len = results.rows.item(0).total;

                if (len > 0) {
                    callback(true);
                } else
                    callback(false);
            });
        });
    },
    updatePassUsuario: function(callback, iusuario, newpassword) {

        console.log("dao:" + iusuario);
        console.log("dao:" + newpassword);
        db.transaction(function(tx) {
            tx.executeSql(UPDATE_PASS_USER, [newpassword, iusuario], function(tx, results) {
                //console.log(results);
                callback("Se cambi&oacute; la contraseña correctamente");
            })
        });
    },
    getPassUsuario: function(callback, iusuario, ipassword) {
        db.transaction(function(tx) {
            //console.log("*******************"+iusuario);
            //console.log("*******************"+ipassword);
            tx.executeSql(SELECT_PASS_USER, [iusuario, ipassword], function(tx, results) {

                console.log('..............>');
                console.log(results.rows.item[0]);

                console.log('..............>');
                callback(results);

            });
        });
    },
    getUsuario: function(callback, iusuario) {
        db.transaction(function(tx) {
            tx.executeSql(SELECT_NAME_USER2, [iusuario], function(tx, results) {
                callback(results);

            });
        });
    },

    updateShowAyuda: function(callback, iusuario, showayuda) {
        db.transaction(function(tx) {
            tx.executeSql(UPDATE_SHOWAYUDA, [showayuda, iusuario], function(tx, results) {
                callback(results);

            });
        });
    }

});