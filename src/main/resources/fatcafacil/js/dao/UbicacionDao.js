var SELECT_DEPARTAMENTOS = "select codubigeo,nomdep from ubicacion where substr(codubigeo, 3, 4) = '0000' order by nomdep asc";
var SELECT_PROVINCIAS = "select codubigeo,nompro from ubicacion where  substr(codubigeo, 3, 2) <> '00' AND substr(codubigeo, 5, 2) = '00' and  substr(codubigeo, 1, 2) =? order by nompro asc";
var SELECT_DISTRITOS = "select codubigeo,nomdis from ubicacion where substr(codubigeo, 5, 2) <> '00' and substr(codubigeo, 1, 4) =? order by nomdis asc";


UbicacionDao = Backbone.Model.extend({
    initialize: function() {
        //console.log('GiinDao');

        this.bind('invalid', function(model, error) {
            console.error(error);
        });
    },
    getDepartamentos: function(callback) {

        db.transaction(function(tx) {

            tx.executeSql(SELECT_DEPARTAMENTOS, [], function(tx, results) {
                //var len = results.rows.length, i;
                //var result = [];*/
                //for (i = 0; i < len; i++) {
                //  console.log(results.rows.item(i));
                //}

                callback(results);
            });
        });
    },
    getDistritos: function(callback, filter) {

        db.transaction(function(tx) {

            tx.executeSql(SELECT_DISTRITOS, [filter], function(tx, results) {
                //var len = results.rows.length, i;
                //var result = [];*/
                //for (i = 0; i < len; i++) {
                //  console.log(results.rows.item(i));
                //}

                callback(results);
            });
        });
    },
    getProvincias: function(callback, filter) {

        db.transaction(function(tx) {

            tx.executeSql(SELECT_PROVINCIAS, [filter], function(tx, results) {
                //var len = results.rows.length, i;
                //var result = [];*/
                //for (i = 0; i < len; i++) {
                //  console.log(results.rows.item(i));
                //}

                callback(results);
            });
        });
    }
});