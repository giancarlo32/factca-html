var SELECT_GIIN = "select codgiin,nombreentidad from GIIN WHERE pais=?";




GiinDao = Backbone.Model.extend({
    initialize: function() {
        console.log('GiinDao');

        this.bind('invalid', function(model, error) {
            console.error(error);
        });
    },
    listagiin: function(callback, codigopais) {
        var filtro = "";
        if (codigopais == "PE") {
            filtro = "PERU";
        }

        if (codigopais == "PA") {
            filtro = "PANAMA";
        }
        if (codigopais == "CL") {
            filtro = "CHILE";
        }
        if (codigopais == "UY") {
            filtro = "URUGUAY";
        }
        if (codigopais == "BB") {
            filtro = "BARBADOS";
        }
        if (codigopais == "BO") {
            filtro = "BOLIVIA";
        }
        if (codigopais == "BM") {
            filtro = "BERMUDAS";
        }
        if (codigopais == "KY") {
            filtro = "ISLAS CAIMAN";
        }


        db.transaction(function(tx) {
            /*var len = results.rows.length, i;
            var result = [];
            for (i = 0; i < len; i++) {
              result[i] = results.rows.item(i);
            }
            callback(result); */
            tx.executeSql(SELECT_GIIN, [filtro], function(tx, results) {
                /*var len = results.rows.length, i;
                var result = [];
                for (i = 0; i < len; i++) {
                  result[i] = results.rows.item(i);
                }*/
                callback(results);
            });
        });
    }
});