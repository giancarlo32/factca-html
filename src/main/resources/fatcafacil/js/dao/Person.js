Person = Backbone.Model.extend({
    initialize: function() {
        console.log('hello world');
        this.bind("change:name", function() {
            console.log(this.get('name') + " is now the value for name");
        });
        this.bind('invalid', function(model, error) {
            console.error(error);
        });
    },
    defaults: {
        name: "Bob Hope",
        height: "unknown"
    },
    validate: function(attributes) {
        if (attributes.name == 'Joe') {
            return "Uh oh, you're name is Joe!";
        }
    },
    findAll: function(callback) {
        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM foo', [], function(tx, results) {
                var len = results.rows.length,
                    i;
                var wines = [];
                for (i = 0; i < len; i++) {
                    console.log(results.rows.item(i).text);
                    wines[i] = results.rows.item(i);
                }
                callback(wines);
            });
        });
    }
});