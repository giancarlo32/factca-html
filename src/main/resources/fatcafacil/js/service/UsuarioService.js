function UsuarioService() {

}
UsuarioService.prototype.checkUsuario = function(funciongeneral, iusuario, ipassword) {


    var ud = new UsuarioDao();
    ud.checkUsuario(function(data) {
        funciongeneral(data);
    }, iusuario, ipassword);
};

UsuarioService.prototype.setUsuario = function(funciongeneral, iusuario, ipassword, inombre, iapellidos, ipais, imodelo, contacto, emailcontacto) {
    //alert("set usuario");
    var ud = new UsuarioDao();
    ud.setUsuario(function(data) {
        funciongeneral(data);
    }, iusuario, ipassword, inombre, iapellidos, ipais, imodelo, contacto, emailcontacto);
};

UsuarioService.prototype.getTotalUsuario = function(funciongeneral) {
    //alert("set usuario");
    var ud = new UsuarioDao();
    ud.getTotalUsuario(function(data) {
        funciongeneral(data);
    });
};

UsuarioService.prototype.getPassUsuario = function(funciongeneral, iusuario, ipassword) {
    var ud = new UsuarioDao();
    ud.getPassUsuario(function(data) {
        funciongeneral(data);
    }, iusuario, ipassword);
};

UsuarioService.prototype.updatePassUsuario = function(finput, iusuario, ipassword, newpassword) {
    //alert("set usuario");
    var ud = new UsuarioDao();
    console.log("********************>>>>>>>>>>>>>>>>>>>>>>");
    console.log(ipassword);
    console.log(iusuario);
    console.log(ipassword);
    console.log(newpassword);

    ud.getPassUsuario(function(data) {
        console.log("********************");
        console.log(data.rows.item[0]);
        console.log("********************");
        if (data.rows.length > 0) {
            ud.updatePassUsuario(function(d) {
                finput(d);
            }, iusuario, newpassword);

        } else {
            finput("Digite correctamente su contrase&ntilde;a actual, para proceder al cambio de contrase&ntilde;a.");
        }

    }, iusuario, ipassword);
};


UsuarioService.prototype.getUsuario = function(callback, iusuario) {
    var ud = new UsuarioDao();

    ud.getUsuario(function(data) {

        callback(data);
    }, iusuario);
};

UsuarioService.prototype.updateShowAyuda = function(callback, iusuario, showayuda) {
    var ud = new UsuarioDao();

    ud.updateShowAyuda(function(data) {
        callback(data);
    }, iusuario, showayuda);
};