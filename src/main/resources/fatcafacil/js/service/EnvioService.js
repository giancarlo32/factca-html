function EnvioService() {

}

EnvioService.prototype.getListaEnvio = function(finput) {
    var ed = new EnvioDao();
    ed.getListaEnvio(function(data) {
        finput(data);
    });
};
EnvioService.prototype.getListaEnvioPorPeriodo = function(finput, anio) {
    var ed = new EnvioDao();
    ed.getListaEnvioPorPeriodo(function(data) {
        finput(data);
    }, anio, global.idusuario);

};

EnvioService.prototype.getListaCuentaPorPeriodo = function(finput, idenvio, pagina, filtro) {
    var ed = new EnvioDao();

    var limit = 10;
    var offset = limit * pagina;

    ed.getListaCuentaPorPeriodo(function(data) {
        finput(data);
    }, idenvio, limit, offset, filtro);

};

EnvioService.prototype.InsertEnvio = function(callback, ANO, fechacreacion, jJsonObj, tipoarchivo, versionfatca, licencia) {
    var ed = new EnvioDao();

    ed.InsertEnvio(function(data) {

        callback(data);
    }, global.idusuario, ANO, fechacreacion, jJsonObj, tipoarchivo, versionfatca, licencia);
};


EnvioService.prototype.getTotalCuentaPorEnvio = function(finput, idenvio, filtro) {
    var ed = new EnvioDao();
    ed.getTotalCuentaPorEnvio(function(data) {
        finput(data);
        console.log(data);
    }, idenvio, filtro);

};
EnvioService.prototype.getListaCuentaBusqueda = function(finput, filtro) {
    var ed = new EnvioDao();

    ed.getListaCuentaBusqueda(function(data) {
        finput(data);
    }, filtro);

};
EnvioService.prototype.getCuentaDetalle = function(finput, idenvio, id) {
    var ed = new EnvioDao();
    ed.getCuentaDetalle(function(data) {
        finput(data);
        console.log("cuentadetalle{");
        console.log(data);
        console.log("}");
    }, idenvio, id);
};




/*EnvioService.prototype.InsertCuenta=function (callback,IDENVIO,propietario,numerocuenta,monto,tipomoneda){
	var ed= new EnvioDao();
	ed.InsertCuenta(function(data){
		callback(data);
	},IDENVIO,propietario,numerocuenta,monto,tipomoneda);
};*/