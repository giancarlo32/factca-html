function ConfiguracionService() {

}


ConfiguracionService.prototype.estadoConfiguracion = function(finput) {
    var ud = new UsuarioDao();
    ud.getTotal(function(result) {
        if (result.total == 0) { //no hay usuario
            finput(1);
        } else {
            finput(3);
        }
    });
};
ConfiguracionService.prototype.crearTablas = function(finput) {

    var cd = new ConfiguracionDao();
    cd.crearTablas(function(data) {
        finput(data);
    });
};

ConfiguracionService.prototype.insertar_entidades = function(finput, jsoninput, idusuario) {

    var cd = new ConfiguracionDao();
    cd.insertar_entidades(function(data) {
        finput(data);
    }, jsoninput, idusuario);
};

ConfiguracionService.prototype.insertar_configuracion = function(finput, jsoninput) {



    var cd = new ConfiguracionDao();

    try {
        fs.mkdirSync(gui.App.dataPath + "\\Files");
        if (jsoninput.path_certificado != "" && jsoninput.actualizar_certificado == 'true') {
            fs.createReadStream(jsoninput.path_certificado).pipe(fs.createWriteStream(gui.App.dataPath + "\\Files" + "\\" + jsoninput['tipocertificado']));
        }
        cd.insertar_configuracion(function(data) {
            finput(data);
        }, jsoninput);
    } catch (e) {
        if (e.code != 'EEXIST') {
            throw e;
        } else {
            if (jsoninput.path_certificado != "" && jsoninput.actualizar_certificado == 'true') {
                fs.createReadStream(jsoninput.path_certificado).pipe(fs.createWriteStream(gui.App.dataPath + "\\Files" + "\\" + jsoninput['tipocertificado']));
            }
            cd.insertar_configuracion(function(data) {
                finput(data);
            }, jsoninput);
        }
    }



};

ConfiguracionService.prototype.select_configuracion = function(finput) {
    var cd = new ConfiguracionDao();
    cd.select_configuracion(function(data) {
        finput(data);
    }, global.idusuario);
};

ConfiguracionService.prototype.prueba = function(funciongeneral) {


    funciongeneral();
};


ConfiguracionService.prototype.prueba = function(funciongeneral, iusuario, ipassword) {


    var ud = new UsuarioDao();
    ud.checkCredenciales(function(data) {
        funciongeneral(data);
    }, iusuario, ipassword);
};

// Giancarlo
ConfiguracionService.prototype.getListaPeriodo = function(finput) {
    var cd = new ConfiguracionDao();
    cd.getListaPeriodo(function(data) {
        finput(data);
    });
};

ConfiguracionService.prototype.getListaPais = function(finput) {
    var cd = new ConfiguracionDao();
    cd.getListaPais(finput);
};


ConfiguracionService.prototype.cargarConfiguracion = function(finput) {
    var cd = new ConfiguracionDao();
    cd.cargarConfiguracion(function(data) {

        jsonObj = {};
        if (data.rows.length > 0) {



            jsonObj['financiera'] = {};
            jsonObj['financiera']["tipoentidad"] = "";
            jsonObj['financiera']["paisorigen"] = "";
            jsonObj['financiera']["nombreentidad"] = "";
            jsonObj['financiera']["codgiin"] = "";
            jsonObj['financiera']["dirpais"] = "";
            jsonObj['financiera']["dirciudad"] = "";
            jsonObj['financiera']["dirprovincia"] = "";
            jsonObj['financiera']["dirdistrito"] = "";
            jsonObj['financiera']["calle"] = "";
            jsonObj['financiera']["numero"] = "";
            jsonObj['financiera']["piso"] = "";
            jsonObj['financiera']["oficina"] = "";
            jsonObj['financiera']["smv"] = 0;

            jsonObj['intermediario'] = {};
            jsonObj['intermediario']["tipoentidad"] = "";
            jsonObj['intermediario']["paisorigen"] = "";
            jsonObj['intermediario']["nombreentidad"] = "";
            jsonObj['intermediario']["codgiin"] = "";
            jsonObj['intermediario']["dirpais"] = "";
            jsonObj['intermediario']["dirciudad"] = "";
            jsonObj['intermediario']["dirprovincia"] = "";
            jsonObj['intermediario']["dirdistrito"] = "";
            jsonObj['intermediario']["calle"] = "";
            jsonObj['intermediario']["numero"] = "";
            jsonObj['intermediario']["piso"] = "";
            jsonObj['intermediario']["oficina"] = "";
            jsonObj['intermediario']["smv"] = 0;

            jsonObj['sponsor'] = {};
            jsonObj['sponsor']["tipoentidad"] = "";
            jsonObj['sponsor']["paisorigen"] = "";
            jsonObj['sponsor']["nombreentidad"] = "";
            jsonObj['sponsor']["codgiin"] = "";
            jsonObj['sponsor']["dirpais"] = "";
            jsonObj['sponsor']["dirciudad"] = "";
            jsonObj['sponsor']["dirprovincia"] = "";
            jsonObj['sponsor']["dirdistrito"] = "";
            jsonObj['sponsor']["calle"] = "";
            jsonObj['sponsor']["numero"] = "";
            jsonObj['sponsor']["piso"] = "";
            jsonObj['sponsor']["oficina"] = "";
            jsonObj['sponsor']["smv"] = 0;


            //console.log(JSON.stringify(data.rows.item[0]));

            for (i = 0; i < data.rows.length; i++) {

                if (data.rows.item(i).tipoentidad == 1) {
                    jsonObj['financiera'] = data.rows.item(i);
                };
                if (data.rows.item(i).tipoentidad == 2) {
                    jsonObj['intermediario'] = data.rows.item(i);
                };
                if (data.rows.item(i).tipoentidad == 3) {
                    jsonObj['sponsor'] = data.rows.item(i);
                };
            }


        }

        finput(jsonObj);
    }, global.idusuario);
};

ConfiguracionService.prototype.ocseJson = function(finput) {


    var cd = new ConfiguracionDao();
    cd.select_configuracion(function(data1) {
        cd.cargarConfiguracion(function(data2) {

            if (data1.rows.length > 0 && data2.rows.length > 0) {

                r = {};
                for (i = 0; i < data2.rows.length; i++) {

                    if (data2.rows.item(i).tipoentidad == 1) {
                        r['financiera'] = data2.rows.item(i);
                    };
                    if (data2.rows.item(i).tipoentidad == 2) {
                        r['intermediario'] = data2.rows.item(i);
                    };
                    if (data2.rows.item(i).tipoentidad == 3) {
                        r['sponsor'] = data2.rows.item(i);
                    };
                }




                /*if(data1.rows.item(0).alias==""||data1.rows.item(0).certificado==""||data1.rows.item(0).pk1==""){
                	mensaje("Debe Configurar el certificado");
                }else*/
                {


                    if (r.financiera.dirciudad === "SELECCIONE") {
                        r.financiera.dirciudad = "";
                    }
                    if (r.financiera.dirprovincia === "SELECCIONE") {
                        r.financiera.dirprovincia = "";
                    }
                    if (r.financiera.dirdistrito === "SELECCIONE" || r.financiera.dirdistrito === "Seleccione") {
                        r.financiera.dirdistrito = "";
                    }


                    JsonOcseJ = {}
                    JsonOcseJ['entidad'] = {};
                    JsonOcseJ['entidad']['nombre'] = r.financiera.nombreentidad;
                    JsonOcseJ['entidad']['giin'] = r.financiera.codgiin;
                    JsonOcseJ['entidad']['pais'] = r.financiera.paisorigen;
                    JsonOcseJ['entidad']['departamento'] = r.financiera.dirciudad;
                    JsonOcseJ['entidad']['provincia'] = r.financiera.dirprovincia;
                    JsonOcseJ['entidad']['distrito'] = r.financiera.dirdistrito;
                    JsonOcseJ['entidad']['calle'] = r.financiera.calle;
                    JsonOcseJ['entidad']['numero'] = r.financiera.numero;
                    JsonOcseJ['entidad']['piso'] = r.financiera.piso;
                    JsonOcseJ['entidad']['oficina'] = r.financiera.oficina;
                    JsonOcseJ['entidad']['smv'] = r.financiera.smv;
                    console.log("ACAAAAA");
                    console.log("*****" + r.financiera.filer + "******");
                    console.log(r.financiera.filer !== "0");
                    console.log(r.financiera.filer != undefined);
                    //JsonOcseJ['entidad']['tipoentidad']=r.financiera.tipoentidad; 
                    if (r.financiera.filer != undefined && r.financiera.filer !== "0") {
                        JsonOcseJ['entidad']['filer'] = r.financiera.filer;
                    }


                    if (r.intermediario != undefined) {

                        if (r.intermediario.dirciudad === "SELECCIONE") {
                            r.intermediario.dirciudad = "";
                        }
                        if (r.intermediario.dirprovincia === "SELECCIONE") {
                            r.intermediario.dirprovincia = "";
                        }
                        if (r.intermediario.dirdistrito === "SELECCIONE" || r.intermediario.dirdistrito === "Seleccione") {
                            r.intermediario.dirdistrito = "";
                        }

                        JsonOcseJ['intermediario'] = {};
                        JsonOcseJ['intermediario']['nombre'] = r.intermediario.nombreentidad;
                        JsonOcseJ['intermediario']['giin'] = r.intermediario.codgiin;
                        JsonOcseJ['intermediario']['pais'] = r.intermediario.paisorigen;
                        JsonOcseJ['intermediario']['departamento'] = r.intermediario.dirciudad;
                        JsonOcseJ['intermediario']['provincia'] = r.intermediario.dirprovincia;
                        JsonOcseJ['intermediario']['distrito'] = r.intermediario.dirdistrito;
                        JsonOcseJ['intermediario']['calle'] = r.intermediario.calle;
                        JsonOcseJ['intermediario']['numero'] = r.intermediario.numero;
                        JsonOcseJ['intermediario']['piso'] = r.intermediario.piso;
                        JsonOcseJ['intermediario']['oficina'] = r.intermediario.oficina;
                        JsonOcseJ['intermediario']['smv'] = r.intermediario.smv;
                    }
                    //JsonOcseJ['intermediario']['tipoentidad']=r.intermediario.tipoentidad; 
                    if (r.sponsor != undefined) {

                        if (r.sponsor.dirciudad === "SELECCIONE") {
                            r.sponsor.dirciudad = "";
                        }
                        if (r.sponsor.dirprovincia === "SELECCIONE") {
                            r.sponsor.dirprovincia = "";
                        }
                        if (r.sponsor.dirdistrito === "SELECCIONE" || r.sponsor.dirdistrito === "Seleccione") {
                            r.sponsor.dirdistrito = "";
                        }

                        JsonOcseJ['sponsor'] = {};
                        JsonOcseJ['sponsor']['nombre'] = r.sponsor.nombreentidad;
                        JsonOcseJ['sponsor']['giin'] = r.sponsor.codgiin;
                        JsonOcseJ['sponsor']['pais'] = r.sponsor.paisorigen;
                        JsonOcseJ['sponsor']['departamento'] = r.sponsor.dirciudad;
                        JsonOcseJ['sponsor']['provincia'] = r.sponsor.dirprovincia;
                        JsonOcseJ['sponsor']['distrito'] = r.sponsor.dirdistrito;
                        JsonOcseJ['sponsor']['calle'] = r.sponsor.calle;
                        JsonOcseJ['sponsor']['numero'] = r.sponsor.numero;
                        JsonOcseJ['sponsor']['piso'] = r.sponsor.piso;
                        JsonOcseJ['sponsor']['oficina'] = r.sponsor.oficina;
                        JsonOcseJ['sponsor']['smv'] = r.sponsor.smv;
                        //JsonOcseJ['sponsor']['tipoentidad']=r.sponsor.tipoentidad; 
                        if (r.sponsor.filer != undefined && r.sponsor.filer !== "0") {
                            JsonOcseJ['sponsor']['filer'] = r.sponsor.filer;
                        }
                    }




                    JsonOcseJ['certificado'] = {};
                    //Incluidos al 20/07/2017
                    if($('#checkWSign').prop('checked')==true){//XML sin Firma
                        JsonOcseJ['incluyeFirma'] = "false";
                    }

                    if($('#checkWOptionals').prop('checked')==true){//XML sin opcionales
                        JsonOcseJ['incluyeOpcional'] = "false";
                    }
                    
                   

                    JsonOcseJ['archivo'] = "";
                    JsonOcseJ['periodo'] = "";
                    JsonOcseJ["directorio"] = gui.App.dataPath + "\\Files" + "\\";
                    JsonOcseJ["alias"] = "f-" + getNameFileDateTime();



                    //certificado		

                    /*JsonOcseJ['certificado']['keystore']=gui.App.dataPath+"\\Files"+"\\"+data1.rows.item(0).certificado;
          	JsonOcseJ['certificado']['alias']=data1.rows.item(0).alias;
          	JsonOcseJ['certificado']['password']=data1.rows.item(0).pk1;
			JsonOcseJ["licencia"]=data1.rows.item(0).LICENCIA;;*/
                    ///////Pais y modelo ///////
                    console.log(data1.rows.item(0));
                    if (data1.rows.item(0).pais == "") {
                        JsonOcseJ["pais"] = "-";
                    } else {
                        JsonOcseJ["pais"] = data1.rows.item(0).pais;
                    }

                    if (data1.rows.item(0).modelo == "") {
                        JsonOcseJ["modelo"] = "";
                    } else {
                        JsonOcseJ["modelo"] = data1.rows.item(0).modelo;
                    }

                    if (data1.rows.item(0).contacto == "" || data1.rows.item(0).contacto == "null" || data1.rows.item(0).contacto == undefined) {
                        //JsonOcseJ["contacto"]="";
                    } else {
                        JsonOcseJ["contacto"] = data1.rows.item(0).contacto;
                    }

                    if (data1.rows.item(0).emailcontacto == "" || data1.rows.item(0).emailcontacto == "null" || data1.rows.item(0).emailcontacto == undefined) {
                        //JsonOcseJ["emailcontacto"]="";
                    } else {
                        JsonOcseJ["emailContacto"] = data1.rows.item(0).emailcontacto;
                    }

                    ///////validacion de la licencia////////////////////
                    var licencia = "trial";
                    if (data1.rows.item(0).alias == "" || data1.rows.item(0).certificado == "" || data1.rows.item(0).pk1 == "" || data1.rows.item(0).alias == undefined || data1.rows.item(0).certificado == undefined || data1.rows.item(0).pk1 == undefined) {
                        //mensaje("Debe Configurar el certificado");
                        //var licencia="trial";
                        JsonOcseJ['certificado']['keystore'] = path.dirname(process.execPath) + "\\certificado\\fatcafacil.p12";
                        JsonOcseJ['certificado']['alias'] = "fatcafacil";
                        JsonOcseJ['certificado']['password'] = "complexless";
                        JsonOcseJ["licencia"] = licencia;;

                    } else {

                        JsonOcseJ['certificado']['keystore'] = gui.App.dataPath + "\\Files" + "\\" + data1.rows.item(0).certificado;
                        JsonOcseJ['certificado']['alias'] = data1.rows.item(0).alias;
                        JsonOcseJ['certificado']['password'] = data1.rows.item(0).pk1;
                        JsonOcseJ["licencia"] = data1.rows.item(0).LICENCIA;;



                        ////////
                        /*var cpto = require('crypto');

		var fs = require('fs');
		 licencia=data1.rows.item(0).LICENCIA;
			if(data1.rows.item(0).LICENCIA==""){
			//var licencia="aCyZW1LU5eueozKtr7940eXNl2xwuBMYFkbrGpelX6Nf3wLXjYL3iVzsSZh16ZdsiqWgtkNonfl/mZHQrIrFevOPScvGuWzDYOyvh6NfBeSmeC67Qisq0sdqdQCs21trkwVbSPLGHUnf3qhts+bqkowEv2LKsvLHXapj0wGmJ2o=";
			var licencia="";
			}
			
				var publicPem = fs.readFileSync(path.dirname( process.execPath )+"\\"+"certificado/pubkey.pem");
						var pubkey = publicPem.toString();
						

						var deencryptedData ="trial";
						try{
						deencryptedData=cpto.publicDecrypt (pubkey,new Buffer(licencia, 'base64'));
						}catch(err) {
    					//alert(err.message);
						}
						for (i = 0; i < data2.rows.length; i++) {
				                 console.log(data2.rows.item(i));
				                  if(data2.rows.item(i).tipoentidad==1){
				                  	
									if(data2.rows.item(i).codgiin==deencryptedData){
										//alert("licencia valida");
									}else{
										//alert("licencia trial");
										licencia="trial";
									}
				                  };
				                   
				         } */

                        ///////////////////////////				         
                        var msg = "";
                        for (i = 0; i < data2.rows.length; i++) {
                            console.log(data2.rows.item(i));
                            if (data2.rows.item(i).tipoentidad == 1) {


                                msg = validarlicenciaDb(data1.rows.item(0).LICENCIA, data2.rows.item(i).codgiin);
                                //alert(msg);
                                /*if(data2.rows.item(i).codgiin==deencryptedData){
                                	//alert("licencia valida");
                                }else{
                                	//alert("licencia trial");
                                	licencia="trial";
                                }*/
                            };

                        }




                        if (msg == "") {
                            licencia = data1.rows.item(0).LICENCIA;
                        } else {
                            licencia = "trial";
                        }

                    }




                    if (licencia == "trial") {
                        JsonOcseJ['entidad']['nombre'] = r.financiera.nombreentidad + "-trial";
                    }



                    JsonOcseJ["licencia"] = licencia;




                    finput(JsonOcseJ, msg);

                }
            } else {
                //mensaje_loading("");
                //mensaje_loading("");
                mensaje("Debe registrar las entidades y configurar el certificado", 3);
            }




        }, global.idusuario);
    }, global.idusuario);




}




ConfiguracionService.prototype.insertPeriodo = function(finput) {
    var cd = new ConfiguracionDao();
    cd.insertPeriodo(function(data) {
        finput(data);
    });
}



ConfiguracionService.prototype.pragma = function(finput) {
    var cd = new ConfiguracionDao();
    cd.pragma(function(data) {
        finput(data);
    }, "ID", "");
}

ConfiguracionService.prototype.actualizar_tablas = function(finput) {
    var cd = new ConfiguracionDao();
    cd.findAll(function(data) {
        //alert("finall");
        cd.get_aplicacion(function(data2) {
            cd.actualizar(data2.rows.item(0).version, finput);

            //alert(data2.rows.item(0).version);


        }, function(data2) {
            //Este es modelo  9.0
            if (data.length == 12) {
                //alert("modelo 0.9");
                mensaje("Se actualiz&oacute; desde la versi&oacute;n 0.9", 1)
                cd.actualizar(9.0, finput);
            } else {
                //alert("modelo 0.0");
                cd.crearTablas(finput);
            }


        });
    });




}