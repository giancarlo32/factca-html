var gui = require('nw.gui');
var win = gui.Window.get();
win.setTransparent(!win.isTransparent);
win.setTransparent(!win.isTransparent);

var path = require('path');
var nwDir = path.dirname(process.execPath);
var paginaactual = "index";

var db = openDatabase('mydb', '1.0', 'my first database', 2 * 1024 * 1024);

var fc = new FatcaController();
var fs = require('fs');
$(document).ready(function($) {
    win.show();
    fc.validarconfiguracion();

    $("#win_maximize").click(function() {
        if (win.isMaximized) {
            win.unmaximize();
        } else {
            win.maximize();
        }
    });

    $("#win_minimize").click(function() {
        win.minimize();
    });

    $("#win_close").click(function() {
        win.close();
    });

    win.on('maximize', function() {
        win.isMaximized = true;
    });

    win.on('unmaximize', function() {
        win.isMaximized = false;
    });
});