function cambio_tipo_archivo() {

    if ($("#tipo_archivo").val() === "VACIO") {
        $("#archivo").prop("disabled", true);


    } else {
        $("#archivo").prop("disabled", false);
        //accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"

        if ($("#tipo_archivo").val() === "CSV") {
            $("#archivo").prop("accept", ".csv,.txt");
        } else if ($("#tipo_archivo").val() === "EXCEL") {
            $("#archivo").prop("accept", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel");
        } else if ($("#tipo_archivo").val() === "XML") {
            $("#archivo").prop("accept", ".xml,.XML");
        }
    }


}

function goconfiguracion(paso) {


    if (paso == 3) {
        global.currentSlide = 3;
        $("#slide1").removeClass("active");
        $("#slide2").removeClass("active");
        $("#slide3").removeClass("active");
        $("#slide3").addClass("active");
        $("#l_ayuda").html(" <h5>Bienvenido a Fatca-fácil</h5><p>Para poder empezar con Fatca-fácil tienes que <strong>crear un usuario</strong>. Este paso es único y quedará grabado en el sistema de Fatca-fácil.</p>");

    }

    if (paso == 2) {
        global.currentSlide = 2;
        $("#slide1").removeClass("active");
        $("#slide2").removeClass("active");
        $("#slide3").removeClass("active");
        $("#slide2").addClass("active");
        $("#l_ayuda").html(" <h5>Bienvenido a Fatca-fácil</h5><p>Para poder empezar con Fatca-fácil tienes que <strong>crear un usuario</strong>. Este paso es único y quedará grabado en el sistema de Fatca-fácil.</p>");

    }
    if (paso == 1) {
        global.currentSlide = 1;
        $("#slide3").removeClass("active");
        $("#slide1").removeClass("active");
        $("#slide2").removeClass("active");
        $("#slide1").addClass("active");
        $("#l_ayuda").html(" <h5>Bienvenido a Fatca-fácil</h5><p>Para poder empezar con Fatca-fácil tienes que escoger el<strong> Pa&iacute;s de la entidad Financiera</strong>. Este paso es único, una vez hecho, <strong>presione continuar</strong> .</p>");
    }
}

/*function mensaje(mensaje){

  //$('#modal_message').modal('toggle');
  //$('#modal_message_message').html(mensaje);
  $.fn.dpToast(mensaje,2000);
   // $().dpToast("Hello World");

}*/
function mensaje_loading(mensaje) {
    $('#modal_message_loading').modal('toggle');
}



function mensaje(mensaje, tipo) {


    // $('#modal_message').modal('toggle');
    // $('#modal_message_message').html(mensaje);
    // $('#modal_message_button').click(f);
    /*
    success
    info
    warning
    danger
    */
    //tipo=3;

    var tipemessage = "";
    if (tipo === 1) {
        tipemessage = "success";
    } else if (tipo === 2) {
        tipemessage = "info";
    } else if (tipo === 3) {
        tipemessage = "warning";
    } else if (tipo === 4) {
        tipemessage = "danger";
    } else {
        tipo = 1;
        tipemessage = "success";
    }


    //$.fn.dpToast(mensaje,2000);
    $.toaster.reset();
    if (tipo === 1) {
        $.toaster({
            settings: {
                toast: {
                    display: function($toast) {
                        return $toast.fadeIn('slow');
                    },
                    remove: function($toast, callback) {
                        return $toast.animate({
                            opacity: '0',
                            height: '0px'
                        }, {
                            duration: 'slow',
                            complete: callback
                        });
                    },
                },
                timeout: 3000,
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil',
            message: mensaje
        });
    }
    if (tipo === 2) {
        $.toaster({
            settings: {
                toast: {
                    display: function($toast) {
                        return $toast.fadeIn('slow');
                    },
                    remove: function($toast, callback) {
                        return $toast.animate({
                            opacity: '0',
                            height: '0px'
                        }, {
                            duration: 'slow',
                            complete: callback
                        });
                    },
                },
                timeout: 4000,
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil - Info',
            message: mensaje
        });
    }
    if (tipo === 3) {
        $.toaster({
            settings: {
                toast: {
                    display: function($toast) {
                        return $toast.fadeIn('slow');
                    },
                    remove: function($toast, callback) {
                        return $toast.animate({
                            opacity: '0',
                            height: '0px'
                        }, {
                            duration: 'slow',
                            complete: callback
                        });
                    },
                },
                timeout: 4000,
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil - Aviso',
            message: mensaje
        });
    }

    if (tipo === 4) {
        $.toaster({
            settings: {
                toast: {
                    fade: { in: 'fast',
                        out: 'slow'
                    },


                    remove: function($toast, callback) {
                        /*alert("remove");
                          return $toast.animate(
                          {
                              opacity: '0',
                              height: '0px'
                          },
                          {
                              duration: settings.toast.fade.out,
                              complete: callback
                          });*/
                    }
                }
            }
        });
        $.toaster({
            priority: tipemessage,
            title: 'Fatca facil - ERROR',
            message: mensaje
        });
    }


}

function open_fatca_configuracion() {

    document.getElementById('certificadofile').value = ""
    $('#edit_certificado').modal('toggle');
    $('#actualizar_certificado').val('false');

    fc.updateConfiguracion();


}


function open_fatca_licencia() {
    fc.cargarConfiguracion();
    fc.updateConfiguracion();
    $('#edit_licencia').modal('toggle');
}

function open_fatca_logo() {

    fc.cargarConfiguracion();
    fc.updateConfiguracion();
    $('#edit_Logo').modal('toggle');

}

function close_form_entidad(n) {



    if (n == 2) {
        $('#check_intermediario').prop('checked', false);
        $('#li_tab_default_2').css('display', 'none');
        //$('#check_intermediario').change();

    }
    if (n == 3) {
        $('#check_sponsor').prop('checked', false);
        $('#li_tab_default_3').css('display', 'none');
        //$('#check_sponsor').change();
    }



    $("#tab_default_1").removeClass("active");
    $("#tab_default_2").removeClass("active");
    $("#tab_default_3").removeClass("active");
    $("#li_tab_default_1").removeClass("active");
    $("#li_tab_default_2").removeClass("active");
    $("#li_tab_default_3").removeClass("active");
    $("#li_tab_default_1").addClass("active");
    $("#tab_default_1").addClass("active");


}




function open_fatca_entidades() {

    $("#tab_default_1").removeClass("active");
    $("#tab_default_2").removeClass("active");
    $("#tab_default_3").removeClass("active");

    $("#li_tab_default_1").removeClass("active");
    $("#li_tab_default_2").removeClass("active");
    $("#li_tab_default_3").removeClass("active");

    $("#li_tab_default_1").addClass("active");
    $("#tab_default_1").addClass("active");



    fc.cargarConfiguracion();
    fc.updateConfiguracion();
    $('#my-Modal-fatca').modal('toggle');

}

function open_subir_archivo() {
    document.getElementById('archivo').value = '';
    $('#upload_dialog').modal('toggle');
    $('#i_select_archivo').val('Haga click para seleccionar un archivo');
}



function open_subir_notificacion() {
    document.getElementById('archivo_notificacion').value = '';
    $('#upload_dialog_notificacion').modal('toggle');
    $('#i_select_archivo_notificacion').val('Haga click para seleccionar un archivo');
}





function cambiarClave() {

    var usuarioLog = global.username;
    //alert(usuarioLog);
    var oldPass = $('#oldPassword').val();
    var pass1 = $('#newPassword1').val();
    var pass2 = $('#newPassword2').val();
    var espacios = false;
    var cont = 0;



    while (!espacios && (cont < pass1.length)) {
        if (pass1.charAt(cont) == " ")
            espacios = true;
        cont++;
    }

    if (espacios) {
        mensaje("La contrase&ntilde;a no puede contener espacios en blanco.", 3);
        return;
    }
    if (pass1 != pass2) {
        mensaje("Las contrase&ntilde;as deben ser iguales.", 3);
        return;
    }
    if (oldPass.length == 0 || pass1.length == 0 || pass2.length == 0) {
        mensaje("Los campos no pueden quedar vac&iacute;os.", 3);
        return;
    }
    if (pass1 == pass2) {

        if (oldPass.length == 0 || pass1.length == 0 || pass2.length == 0) {
            mensaje("Los campos no pueden quedar vac&iacute;os.", 3);
            return false;
        } else {
            fc.updatePassUsuario(oldPass, usuarioLog, pass1, pass2);
            $('#my-Modal').modal('toggle')
            clearFieldsPass();


        }
    }
}

function clearFieldsPass() {
    $('#oldPassword').val("");
    $('#newPassword1').val("");
    $('#newPassword2').val("");

}

function cerrar_sesion() {
    global.isLogged = false;
    /*$("#workspace").load("html/login.html", function() {
                        });*/
    window.location.reload(true);
}

function load_main() {



    if (global.pais == "PA") {
        $("#workspace").load("html/main_pa.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }



            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            // fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();

        });
    }
    //Datos agregados chile
    if (global.pais == "CL") {
        $("#workspace").load("html/main_cl.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }



            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            // fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();

        });
    }

    if (global.pais == "PE") {
        $("#workspace").load("html/main.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }




            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();
            //alert('main');

        });
    }
    if (global.pais == "UY") {
        $("#workspace").load("html/main_uy.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }




            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();
            //alert('main');

        });
    }
    if (global.pais == "BB") {
        $("#workspace").load("html/main_bb.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }




            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();
            //alert('main');

        });
    }
    if (global.pais == "BO") {
        $("#workspace").load("html/main_bo.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }




            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();
            //alert('main');

        });
    }
    if (global.pais == "BM") {
        $("#workspace").load("html/main_bm.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }




            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();
            //alert('main');

        });
    }
    if (global.pais == "KY") {
        $("#workspace").load("html/main_ky.html", function() {
            if (global.username.length > 16) {
                $("#l_nombres_usuario").html(global.username.substring(0, 13) + "...");
            } else {
                $("#l_nombres_usuario").html(global.username);
            }

            if (global.nombres.length > 50) {
                $(".user_name").html(global.nombres.substring(0, 50).toUpperCase() + "...");
            } else {
                $(".user_name").html(global.nombres.toUpperCase());
            }

            var cs = new ConfiguracionService();
            cs.insertPeriodo(function() {
                fc.getListaPeriodo();
            });
            fc.getDepartamentos();
            fc.updateConfiguracion();
            fc.cargarConfiguracion();
            fc.checkUpdates();

        });
    }


}

function getDateTime() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month.toString().length == 1) {
        var month = '0' + month;
    }
    if (day.toString().length == 1) {
        var day = '0' + day;
    }
    if (hour.toString().length == 1) {
        var hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        var minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        var second = '0' + second;
    }
    var dateTime = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    return dateTime;
}


function getNameFileDateTime() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month.toString().length == 1) {
        var month = '0' + month;
    }
    if (day.toString().length == 1) {
        var day = '0' + day;
    }
    if (hour.toString().length == 1) {
        var hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        var minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        var second = '0' + second;
    }
    var dateTime = year + '-' + month + '-' + day + '-' + hour + '-' + minute + '-' + second;
    return dateTime;
}


function checkClave() {
    //alert("Entro a la funcion checkclave")
    var pass1 = document.getElementById('usuario_creacion_password').value;
    var pass2 = document.getElementById('usuario_creacion_repassword').value;
    var usuario = document.getElementById('usuario_creacion_usuario').value;
    var nombre = document.getElementById('usuario_creacion_nombres').value;
    var apellido = document.getElementById('usuario_creacion_apellidos').value;




    var pais = $("#pais").val();
    var modelo = $("#modelo").val();

    var contacto = $("#nombrecontacto").val();
    var emailcontacto = $("#correocontacto").val();


    var espacios = false;
    var cont = 0;

    if (usuario.length == 0 || nombre.length == 0 || apellido.length == 0) {
        mensaje("Los campos no pueden quedar vac&iacute;os.", 3);
        return;
    }


    if (usuario.length <= 2) {
        mensaje("El usuario debe tener mas de 4 caracteres.", 3);
        return;
    }
    if (nombre.length <= 2 || apellido.length <= 2) {
        mensaje("Los nombres y apellidos deben tener mas de 4 caracteres", 3);
        return;
    }
    if (nombre.length <= 2 || apellido.length <= 2) {
        mensaje("Los nombres y apellidos deben tener mas de 4 caracteres");
        return;
    }
    if (pass1.length <= 2 || pass2.length <= 2) {
        mensaje("La contrase&ntilde;a debe tener 6 o m&aacute;s caracteres", 3);
        return;
    }

    if (contacto.length <= 2) {
        mensaje("Debe introducir el nombre del contacto", 3);
        return;
    }

    if (emailcontacto.length <= 2) {
        mensaje("Debe introducir el email del contacto", 3);
        return;
    }

    while (!espacios && (cont < pass1.length)) {
        if (pass1.charAt(cont) == " ")
            espacios = true;
        cont++;
    }

    if (espacios) {
        mensaje("La contrase&ntilde;a no puede contener espacios en blanco.", 3);
        return;
    }

    if (pass1.length == 0 || pass2.length == 0) {
        mensaje("Los campos de la contrase&ntilde;a no pueden quedar vac&iacute;os.", 3);
        return;
    }

    if (pass1 != pass2) {
        mensaje("Las contrase&ntilde;as deben ser iguales.", 3);
        return;
    }
    if (pass1 == pass2) {
        fc.setUsuario(usuario, pass1, nombre, apellido, pais, modelo, contacto, emailcontacto);


    }

}


function alfanumerico(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "áéíóúabcdefghijklmnñopqrstuvwxyz0123456789";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function onlyLetters(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function openUrl() {
    var gui = require('nw.gui');
    gui.Shell.openExternal('http://www.fatcafacil.com');
    gui.Shell.openExternal('http://docs.fatcafacil.com');
}


function buscarTexto() {

    if ($("#i_envio_text").val() != "") {
        fc.getListaCuentaPorPeriodo($("#i_envio_text").val(), 0);
    }

}

function validarlicencia() {




    var codgin_financiera = $("#entidadcodgiin").val();

    var licenciai = $("#licencia").val();
    if (codgin_financiera != "") {

        if (licenciai == "") {
            fc.insertar_configuracion();
            $('#edit_licencia').modal('toggle')
        } else {
            var cpto = require('crypto');
            var fs = require('fs');
            var deencryptedData = "trial";
            //var publicPem = fs.readFileSync(path.dirname( process.execPath )+"\\"+"certificado/pubkey.pem");
            //var pubkey = publicPem.toString();
            var pubkey = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFO15S7ZTCaMe09VY3QATc67K6\n8zrIJgyw8BdJfL+TK+PqN0JMscSB/DJ5J9/8P3UIER8nMfH3AKqgEYR0knm0tifH\nFEcMX8LADHzCoNoVfuN0vabwNvQWoMZaLbV3o/0cdq/Zpl591Q2cc2P7s69nxMxi\n7wL5gcxbroqWlwhPaQIDAQAB\n-----END PUBLIC KEY-----";



            try {
                deencryptedData = cpto.publicDecrypt(pubkey, new Buffer(licenciai, 'base64'));
            } catch (err) {
                deencryptedData = '{"codgiin":"trial","anios":[]}';
            }


            var deencryptedGiin = "";
            try {
                deencryptedGiin = jQuery.parseJSON(deencryptedData).codgiin;
            } catch (err) {
                deencryptedGiin = "trial";
            }

            var anios = "";
            try {
                anios = jQuery.parseJSON(deencryptedData).anios;
            } catch (err) {
                anios = [];
            }



            var tiene_anio_vigente = false;

            console.log(anios);
            for (var i = 0, len = anios.length; i < len; i++) {
                console.log(anios[i]);
                if (anios[i] == gui.App.manifest.year) {
                    tiene_anio_vigente = true;
                }
            }



            if (deencryptedGiin == codgin_financiera) {

                if (tiene_anio_vigente == true) {
                    fc.insertar_configuracion();
                    $('#edit_licencia').modal('toggle')
                } else {
                    mensaje("La licencia no es v&aacute;lida para el a&ntilde;o " + gui.App.manifest.year, 3);
                }

            } else {
                mensaje("La licencia introducida no es una licencia válida para el GIIN " + codgin_financiera, 3)
            }
        }
    } else {
        mensaje("Configurar la entidad financiera", 3);
    }

}

function mostrarestadolicencia(cs) {
    cs.cargarConfiguracion(function(r) {

        if (r.financiera != undefined && r.financiera.codgiin != undefined) {

            if (validarlicenciaDb($("#licencia").val(), r.financiera.codgiin) == "") {
                $("#caption").html("FatcaF&aacute;cil versi&oacute;n " + gui.App.manifest.version + " - activado");
            } else {
                $("#caption").html("FatcaF&aacute;cil versi&oacute;n " + gui.App.manifest.version + " - trial");
            }
        } else {
            $("#caption").html("FatcaF&aacute;cil versi&oacute;n " + gui.App.manifest.version + " - trial");
        }
    });

}


function validarlicenciaDb(licenciai, codgin_financiera) {


    var msg = "";


    if (codgin_financiera != "") {

        if (licenciai == "") {
            msg = "Debe configurar la licencia";
        } else {
            var cpto = require('crypto');
            var fs = require('fs');
            var deencryptedData = "trial";
            //var publicPem = fs.readFileSync(path.dirname( process.execPath )+"\\"+"certificado/pubkey.pem");
            //var pubkey = publicPem.toString();
            var pubkey = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFO15S7ZTCaMe09VY3QATc67K6\n8zrIJgyw8BdJfL+TK+PqN0JMscSB/DJ5J9/8P3UIER8nMfH3AKqgEYR0knm0tifH\nFEcMX8LADHzCoNoVfuN0vabwNvQWoMZaLbV3o/0cdq/Zpl591Q2cc2P7s69nxMxi\n7wL5gcxbroqWlwhPaQIDAQAB\n-----END PUBLIC KEY-----";



            try {
                deencryptedData = cpto.publicDecrypt(pubkey, new Buffer(licenciai, 'base64'));
            } catch (err) {
                deencryptedData = '{"codgiin":"trial","anios":[]}';
            }

            var deencryptedGiin = jQuery.parseJSON(deencryptedData).codgiin;
            var anios = jQuery.parseJSON(deencryptedData).anios;
            var tiene_anio_vigente = false;


            for (var i = 0, len = anios.length; i < len; i++) {

                if (anios[i] == gui.App.manifest.year) {
                    tiene_anio_vigente = true;
                }
            }



            if (deencryptedGiin == codgin_financiera) {

                if (tiene_anio_vigente == true) {

                } else {
                    msg = "La licencia no es v&aacute;lida para el a&ntilde;o " + gui.App.manifest.year;
                }

            } else {
                msg = "La licencia introducida no es una licencia válida para el GIIN " + codgin_financiera;
            }
        }
    } else {
        msg = " Debe Configurar la entidad financiera";
    }

    return msg;

}


function read_alias() {
    read_alias_from_pfx(function() {});
}


function read_alias_from_pfx(f) {

    var file_certificado;




    try {
        file_certificado = $("#certificadofile")[0].files[0].path;
        $('#certificado_actual').val($('#certificadofile')[0].files[0].name);
        $('#actualizar_certificado').val('true');

    } catch (ee) {
        file_certificado = gui.App.dataPath + "\\Files" + "\\" + $('#certificado_actual').val();
    }




    var password = $("#passwordkey").val();
    $("#alias").html("");


    if (file_certificado != "" && password != "") {




        //mensaje_loading("");
        var result = "[]";

        ///////////////CQ
        var path = require('path');



        var path_java = '"' + path.dirname(process.execPath) + "/Runtime/bin/java" + '"';
        var java_lib = '"' + path.dirname(process.execPath) + "/Runtime/lib/jff.jar " + '" ';
        var parameters = " -jar " + java_lib + ' -a "' + file_certificado + '" "' + password + '"';



        ///////Solo para MAC ////////
        //java_home = "";
        //parameters = " -jar /Users/ceqs/Desarrollos/Eclipse/Java/complexless-fatca-util-v1/build/jff.jar " + outputFilename;
        /////////////////////////////

        console.log(path_java);
        var cmd = path_java + parameters;
        console.log(cmd);

        var exec = require('child_process').exec,
            child;
        child = exec(cmd,
            function(error, stdout, stderr) {

                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    mensaje("Ocurri&oacute; un error desconocido", 4)
                } else {

                    var jJsonObj = jQuery.parseJSON(stdout);

                    if (jJsonObj.codigoError == 0) {




                        /* es.InsertEnvio(function(m){
                           if(tipolicencia==""){
                               mensaje(m);
                             }else{
                               mensaje(m+" Se ha utilizado una licencia trial. "+msg+" para mayor informaci&oacute;n visite <a href='#' onclick='gui.Shell.openExternal(\"http://fatcafacil.com/\");'>www.fatcafacil.com</a>");
                             }
                           fc.getListaEnvioPorPeriodo($('#tipo_periodo').val()); 
                         },$('#tipo_periodo').val(),'',jJsonObj,$('#tipo_archivo').val());*/


                        if (jJsonObj.alias.length > 1) {
                            $("#alias").append("<option value=0>SELECCIONE</option>");
                            for (i = 0; i < jJsonObj.alias.length; i++) {
                                $("#alias").append("<option value=" + (i + 1) + ">" + jJsonObj.alias[i] + "</option>");
                            }
                        } else if (jJsonObj.alias.length == 1) {
                            $("#alias").append("<option value=" + (1) + ">" + jJsonObj.alias[0] + "</option>");
                        }

                    } else {
                        //mensaje(jJsonObj.mensajeError);
                        $("#alias").append("<option value=0>ERROR: PASSWORD Y CERTIFICADO</option>");
                        //$('#actualizar_certificado').val('false');
                        //$('#certificado_actual').val('');
                        //document.getElementById('certificadofile').value = ""
                    }

                }
                //mensaje_loading("");

                f();


            });

    }

}

function open_poolreporte(idenvio) {

    var modalOp = false; // Si abre el modal de una opcion u otra
    var opr = false; //Si existe reporte grupal
    var opn = false; // Si existe reporte nulo 


    var t_var = $("#informe_envio_" + idenvio).val();
    //alert(t_var);
    $('#informes-table-div').html(' <table id="informes-table" class="table table-hover"><thead ><th style="  vertical-align: middle;width:100px" data-dynatable-column="identificador">Identificador</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="balance">Balance</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="cantidadCuentas">N&uacute;mero de cuentas</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="tipoGrupal">Tipo Grupo</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="tipoMoneda">Tipo Moneda</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="tipoReporte">Tipo de reporte</th></thead><tbody></tbody></table>');

    var t_var2 = $("#informe_nulo_" + idenvio).val();
    $('#nulo-table-div').html(' <table id="nulo-table" class="table table-hover"><thead ><th style="  vertical-align: middle;width:100px" data-dynatable-column="identificador">Identificador</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="correlativo">Correlativo</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="referencia">Referencia</th><th style="  vertical-align: middle;width:100px" data-dynatable-column="tipoReporte">Tipo de reporte</th></thead><tbody></tbody></table>');

    console.log("open pool ");
    console.log(t_var);
    console.log("[" + t_var2 + "]");
    if (t_var != 'null' && t_var != '[]') {
        modalOp = true;
        opr = true;
        $('#detalle_poolreporte').modal('toggle');
        $('#informes-table').dynatable({
            table: {
                defaultColumnIdStyle: 'trimDash'
            },
            features: {
                paginate: false,
                search: false,
                recordCount: false,
                perPageSelect: false
            },
            dataset: {
                records: JSON.parse(t_var)
            }
        });
    } else if (t_var2 != 'null' && t_var2 != '[]') {
        if (!modalOp) {
            $('#detalle_poolreporte').modal('toggle');
        }
        opn = true;
        $('#nulo-table').dynatable({
            table: {
                defaultColumnIdStyle: 'trimDash'
            },
            features: {
                paginate: false,
                search: false,
                recordCount: false,
                perPageSelect: false
            },
            dataset: {
                records: JSON.parse("[" + t_var2 + "]")
            }
        });
    } else {
        //mensaje("No hay informes para este env&iacute;o",2);
        console.log("aqui");
    }

    //Mejorar con respecto al diseño de los tabs, 
    $('#li_tab_reporte_1').hide();
    $('#li_tab_reporte_2').hide();
    if (opn) {
        $('#tab_reporte_1').hide();
        $('#tab_reporte_2').show();
    }
    if (opr) {
        $('#tab_reporte_2').hide();
        $('#tab_reporte_1').show();
    }

    /*if()
$('#propietario-table').dynatable({
table: {
    defaultColumnIdStyle: 'trimDash'
  },
  features: {
    paginate: false,
    search: false,
    recordCount: false,
    perPageSelect: false
  },
   dataset: {
    records:  obj.propietarios
  }
});*/



}


function getDetalles(idenvio, idcuenta) {
    //alert(idenvio);
    clearDetalle();
    var es = new EnvioService();
    var info = "";
    es.getCuentaDetalle(function(data) {
        var len = data.rows.length,
            i;
        for (i = 0; i < len; i++) {

            //console.log(data.rows.item(i).informacion);
            if (data.rows.item(i).informacion != undefined)
                info = data.rows.item(i).informacion.toString();

        }
        if (info != "") {
            var obj = JSON.parse(info);
            console.log("-----;3");
            console.log(obj);
            console.log("-----:3")
            var nomb = "Nombre"

            $('#tab_detcuenta_1').removeClass('active');
            $('#tab_detcuenta_2').removeClass('active');
            $('#tab_detcuenta_3').removeClass('active');
            $('#tab_detcuenta_4').removeClass('active');
            $('#tab_detcuenta_5').removeClass('active');

            $('#li_tab_detcuenta_1').removeClass('active');
            $('#li_tab_detcuenta_2').removeClass('active');
            $('#li_tab_detcuenta_3').removeClass('active');
            $('#li_tab_detcuenta_4').removeClass('active');
            $('#li_tab_detcuenta_5').removeClass('active');


            $('#tab_detcuenta_1').addClass('active');
            $('#li_tab_detcuenta_1').addClass('active');

            $('#detalle_cuenta').modal('toggle');



            if (obj.tipoReporte === null || obj.tipoReporte === "" || obj.tipoReporte === undefined) {
                obj.tipoReporte = "";
            }
            $("#detalle_tiporeporte_cuenta").append("<div>" + obj.tipoReporte + "</div>");

            if (obj.identificador === null || obj.identificador === "" || obj.identificador === undefined) {
                obj.identificador = "";
            }
            $("#detalle_identificador").append("<div>" + obj.identificador + "</div>");

            if (obj.numeroCuenta === null || obj.numeroCuenta === "" || obj.numeroCuenta === undefined) {
                obj.numeroCuenta = "";
            }
            $("#detalle_numero_cuenta").append("<div>" + obj.numeroCuenta + "</div>");

            if (obj.nombre === null || obj.nombre === "" || obj.nombre === undefined) {
                obj.nombre = "";
            }
            $("#detalle_nombre").append("<div>" + obj.nombre + "</div>");

            if (obj.apellidos === null || obj.apellidos === "" || obj.apellidos === undefined) {
                obj.apellidos = "";
            }
            $("#detalle_apellido").append("<div>" + obj.apellidos + "</div>");


            var t_prop = null;
            if (obj.tipoPropietario == 1) {
                t_prop = "Individual";
            }
            if (obj.tipoPropietario == 2) {
                t_prop = "Organizacion";
            }
            if (t_prop === undefined || t_prop === null) {
                t_prop = "";
            }
            $("#detalle_tipopropietario_cuenta").append("<div>" + t_prop + "</div>");

            if (obj.tipoMoneda === null || obj.tipoMoneda === "" || obj.tipoMoneda === undefined) {
                obj.tipoMoneda = "";
            }
            $("#detalle_balance").append("<div>" + obj.balance + " " + obj.tipoMoneda + "</div>");

            if (obj.fechaNacimiento === null || obj.fechaNacimiento === "" || obj.fechaNacimiento === undefined) {
                obj.fechaNacimiento = "";
            }
            $("#detalle_fecha_nacimiento").append("<div>" + obj.fechaNacimiento + "</div>");

            if (obj.paisResidencia === null || obj.paisResidencia === "" || obj.paisResidencia === undefined) {
                obj.paisResidencia = "";
            }
            $("#detalle_pais_residencia").append("<div>" + obj.paisResidencia + "</div>");

            if (obj.lugarEmision === null || obj.lugarEmision === "" || obj.lugarEmision === undefined) {
                obj.lugarEmision = "";
            }
            $("#detalle_lugar_emision").append("<div>" + obj.lugarEmision + "</div>");

            if (obj.nacionalidad === null || obj.nacionalidad === "" || obj.nacionalidad === undefined) {
                obj.nacionalidad = "";
            }
            $("#detalle_nacionalidad").append("<div>" + obj.nacionalidad + "</div>");

            if (obj.paisDireccion === null || obj.paisDireccion === "" || obj.paisDireccion === undefined) {
                obj.paisDireccion = "";
            }
            $("#detalle_direccion_pais").append("<div>" + obj.paisDireccion + "</div>");

            if (obj.textoDireccion === null || obj.textoDireccion === "" || obj.textoDireccion === undefined) {
                obj.textoDireccion = "";
            }
            $("#detalle_direccion_pais_texto").append("<div>" + obj.textoDireccion + "</div>");

            if (obj.tin === null || obj.tin === "" || obj.tin === undefined) {
                obj.tin = "";
            }
            $("#detalle_tin").append("<div>" + obj.tin + " - " + obj.lugarEmision + "</div>");




            jsonProp = []; // Json para propiedades.
            for (var pr = 0 in obj.propietarios) {

                var name = obj.propietarios[pr].nombre;
                var apell = obj.propietarios[pr].apellidos;
                var tinprop = obj.propietarios[pr].tin;
                var lugarEmi = obj.propietarios[pr].lugarEmision;
                var tipoPropietario_prop = obj.propietarios[pr].tipoPropietario; //GVR 15-02-2017 
                var typeTextOwner = "";

                if (obj.propietarios[pr].nombre === null || obj.propietarios[pr].nombre === "" || obj.propietarios[pr].nombre === undefined) {
                    name = "";
                }
                if (obj.propietarios[pr].apellidos === null || obj.propietarios[pr].apellidos === "" || obj.propietarios[pr].apellidos === undefined) {
                    apell = "";
                }
                if (obj.propietarios[pr].tin === null || obj.propietarios[pr].tin === "" || obj.propietarios[pr].tin === undefined) {
                    tinprop = "";
                }
                if (obj.propietarios[pr].lugarEmision === null || obj.propietarios[pr].lugarEmision === "" || obj.propietarios[pr].lugarEmision === undefined) {
                    lugarEmi = "";
                }

                if (tipoPropietario_prop === null || tipoPropietario_prop === "" || tipoPropietario_prop === undefined || tipoPropietario_prop === "1") { //GVR 15-02-2017 
                    typeTextOwner = "Individual";
                } else if (tipoPropietario_prop === "2") {
                    typeTextOwner = "Organizacion"
                }

                prop = {};
                prop["tipo"] = typeTextOwner; //GVR 15-02-2017 
                prop["nombres"] = name + " " + apell;
                prop["tin"] = tinprop + " - " + lugarEmi;
                jsonProp.push(prop);
            }


            var dynatable = $('#propietario-table').dynatable({
                table: {
                    defaultColumnIdStyle: 'trimDash'
                },
                features: {
                    paginate: false,
                    search: false,
                    recordCount: false,
                    perPageSelect: false
                },
                dataset: {
                    records: jsonProp
                }
            }).data('dynatable');

            dynatable.settings.dataset.originalRecords = jsonProp;
            dynatable.process();


            var dynatable2 = $('#saldo-table').dynatable({
                table: {
                    defaultColumnIdStyle: 'trimDash'
                },
                features: {
                    paginate: false,
                    search: false,
                    recordCount: false,
                    perPageSelect: false
                },
                dataset: {
                    records: obj.pagos
                }
            }).data('dynatable');


            dynatable2.settings.dataset.originalRecords = obj.pagos;
            dynatable2.process();


            if (obj.car !== undefined) {
                console.log("tiene autoridad");
                $('#li_tab_detcuenta_4').show();
                var jsonAuto = '[{"tipo":"' + obj.car.tipoReporte + '","id":"' + obj.car.identificador + '","numero":"' + obj.car.correlativo + '"}]';
                var response = JSON.parse(jsonAuto);
                var dynatable3 = $('#autoridad-table').dynatable({
                    table: {
                        defaultColumnIdStyle: 'trimDash'
                    },
                    features: {
                        paginate: false,
                        search: false,
                        recordCount: false,
                        perPageSelect: false
                    },
                    dataset: {
                        records: response
                    }
                }).data('dynatable');


                dynatable3.settings.dataset.originalRecords = response;
                dynatable3.process();
            } else {
                console.log("no tiene autoridad");
                $('#li_tab_detcuenta_4').hide();
            }



            if (obj.adicionales !== undefined) {

                $('#li_tab_detcuenta_5').show();
                var dynatable4 = $('#adicional-table').dynatable({
                    table: {
                        defaultColumnIdStyle: 'trimDash'
                    },
                    features: {
                        paginate: false,
                        search: false,
                        recordCount: false,
                        perPageSelect: false
                    },
                    dataset: {
                        records: obj.adicionales
                    }
                }).data('dynatable');


                dynatable4.settings.dataset.originalRecords = obj.adicionales;
                dynatable4.process();
            } else {
                console.log("no tiene adicionales");
                $('#li_tab_detcuenta_5').hide();
            }




        } else {
            mensaje("No se puede mostrar informaci&oacute;n para esta cuenta descargue el xml respectivo para mayor informaci&oacute;n", 2)
        }
    }, idenvio, idcuenta);


}

function clearDetalle() {

    //$("#detalle_numero_cuenta").html("");
    $("#detalle_tiporeporte_cuenta").html("");
    $("#detalle_nombre").html("");
    $("#detalle_apellido").html("");
    $("#detalle_numero_cuenta").html("");
    $("#detalle_balance").html("");
    $("#detalle_fecha_nacimiento").html("");
    $("#detalle_identificador").html("");
    $("#detalle_lugar_emision").html("");
    $("#detalle_nacionalidad").html("");
    $("#detalle_direccion_pais").html("");

    $("#detalle_direccion_pais").html("");
    $("#detalle_direccion_pais_texto").html("");
    $("#detalle_tin").html("");
    $("#detalle_pais_residencia").html("");
    $("#detalle_tipopropietario_cuenta").html("");


}

function isNulo(s) {
    if (s != "" && s != "null" && s != undefined) {
        return false;
    } else
        return true;
}


function openPlantilla() {
    var exec = require('child_process').exec,
        child;
    child = exec('start excel ' + '"' + path.dirname(process.execPath) + '\\plantillafatca.xls"');
}

function guardarcertificado() {
    //alert($("#alias").val());
    if ($("#alias").val() != null && $("#alias").val() != 0) {
        fc.insertar_configuracion();
        $('#edit_certificado').modal('toggle')
    } else {
        mensaje("No colocado un password correcto porque no puede seleccionar un alias", 3);
    }
}

/*

function verReporte(){


var dynatable3 = $('#informes-table').dynatable({
table: {
    defaultColumnIdStyle: 'trimDash'
  },
  features: {
    paginate: false,
    search: false,
    recordCount: false,
    perPageSelect: false
  },
   dataset: {
    records:  obj.informes
  }
}).data('dynatable');

dynatable3.settings.dataset.originalRecords = obj.informes;
dynatable3.process();
}*/